#!/usr/bin/python
"""
One-off Wikidata script to remove P2580 (Baltisches Biographisches Lexikon) from references 
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2022
# (C) William Avery, 2022
#
# Distributed under the terms of the MIT license.
#

import json

import pywikibot
from pywikibot import pagegenerators
import pagegenerators_ext
from pywikibot.page import WikibasePage
from pywikibot.bot import (
    WikidataBot
)

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {'&params;': pagegenerators.parameterHelp}


class RemoveRefsByProvenance(
    WikidataBot,  # Only used to get user_edit_entity method for testing, treat_page is overridden
):

    def __init__(self, **kwargs):
        self.available_options.update({
        })

        super(RemoveRefsByProvenance, self).__init__(**kwargs)

        self.options = kwargs

        self.target = 'P2580'

    def teardown(self):
        pass

    def treat_page(self) -> None:
        
        p_item = self.current_page

        delete_count = 0

        for prop in self.current_page.claims:
            claim_list = self.current_page.claims.get(prop)
            for claim in claim_list:
                for source in claim.sources:
                    if self.target in source:
                                pywikibot.output(f"Processing ({prop})")
                                del source[self.target]
                                delete_count+=1

        if not delete_count: return True 

        summary = f"Removing Baltisches Biographisches Lexikon digital ID (former scheme) in accordance with [[Wikidata:Properties for deletion/P2580]] (×{delete_count})"

        # 'Always update' option is assumed to indicate bot operation
        if "always" in self.options and self.options["always"]:
            summary += ". See [[Wikidata:Requests_for_permissions/Bot/William_Avery_Bot_4|request for permission]]"
            self.current_page.editEntity(summary=summary)
        else:
            self.user_edit_entity(self.current_page, summary=summary)
        
def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {}

    local_args = pywikibot.handle_args(args)

    gen_factory = pagegenerators_ext.GeneratorFactory()

    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(":")
        option = arg[1:]
        options[option] = True

    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        options["generator"] = gen
        bot = RemoveRefsByProvenance(**options)
        bot.run() 
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == '__main__':
    main()
