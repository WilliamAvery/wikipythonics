use s54354__batchcontrol;

CREATE TABLE treccani_batch (
    item nvarchar(16) not null,
    scheduled_time datetime,
    processed_time datetime
);

alter table treccani_batch add primary key (item);

create index proc_index using btree on  treccani_batch (processed_time, scheduled_time);

