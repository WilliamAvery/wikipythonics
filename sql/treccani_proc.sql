use s54354__batchcontrol;
DELIMITER //
CREATE PROCEDURE batchcontrol_tr (IN item_qid char(10))
 BEGIN
  UPDATE treccani_batch set processed_time = NOW() WHERE item=item_qid;
  COMMIT;
 END;
//
