use s54354__batchcontrol;

CREATE TABLE prh_batch (
    id            int(10) unsigned,
    namespace     int(11), 
    title         varbinary(255),
    insert_time datetime,
    last_proc_time datetime,
    status  tinyint, /* 0 = ready, 1 = processed, 2 = blocked */
    flag1   tinyint,
    flag2   tinyint,
    flag3   tinyint
);

alter table prh_batch add primary key (id);

create index prh_status using btree on  prh_batch (status);