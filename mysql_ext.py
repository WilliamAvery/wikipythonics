"""
Extra functions to run within pywikibot framework
This is a module to be imported, not a script to be run 
"""
#
# Based on pywikibot's mysql.py (C) Pywikibot team, 2016-2021
# (C) William Avery 2021
#
# Distributed under the terms of the MIT license.
#
from typing import Optional
from pywikibot.data import mysql
import pkg_resources

import pywikibot
from pywikibot import config

try:
    import pymysql
except ImportError:
    raise ImportError("MySQL python module not found. Please install PyMySQL.")


def mysql_procedure(
    query: str,
    params=None,
    dbname: Optional[str] = None,
    verbose: Optional[bool] = None,
):
    """Execute a database procedure"""
    if verbose is None:
        verbose = config.verbose_output

    if config.db_connect_file is None:
        credentials = {"user": config.db_username, "password": config.db_password}
    else:
        credentials = {"read_default_file": config.db_connect_file}

    connection = pymysql.connect(
        host=config.db_hostname_format.format(dbname),
        database=config.db_name_format.format(dbname),
        port=config.db_port,
        charset="utf8",
        **credentials
    )

    with connection as conn, conn.cursor() as cursor:
        return cursor.callproc(query, params)
