# README #
Python scripts that use pywikibot along with some yaml and bash scripts to run them.

As of 2021-12-01 I am using Python version 3.9, with copious f-strings

# Wikidata Tasks

## 1 - Cronjob linking enwiki articles to Wikidata items based on taxonbar

1. taxonbarSyncerBot.py
2. yaml/taxonbardesyncer.yaml

## 9 - Cronjob sanitising reference URLs

1. cleanseRefs.py
2. runurlcleanser.py
3. yaml/urlcleanser.yaml

