#!/usr/bin/python
"""
Bot to add additional Treccani IDs to  a Wikidata item, when one is already present
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2021
# (C) William Avery, 2021
#
# Distributed under the terms of the MIT license.
#
# Requires pywikibot version with fix for https://phabricator.wikimedia.org/T308245
#

import json
import requests
import re
from time import sleep, time
from datetime import datetime
from urllib import parse
from bs4 import BeautifulSoup
import pywikibot as pwb
from pywikibot import Timestamp, WbTime, Page, ItemPage, PropertyPage
import pagegenerators_ext
from pywikibot.bot import (
    # ExistingPageBot,
    WikidataBot,
    # SingleSiteBot
)
from pywikibot.data import sparql
from pywikibot.page._collections import (
    ClaimCollection,
)

class treccaniScraper(
    WikidataBot,  # Only used to get user_edit_entity method for testing, treat_page is overridden
):
    def __init__(self, **kwargs):
        self.available_options.update({
            'report_page': None,  # name of a page to log claims added
            })

        super(treccaniScraper, self).__init__(**kwargs)
        self.options = kwargs
        self.lastRequest = 0
        self.last_url = None
        self.last_soup = None

    def teardown(self):
        pass

    def treccaniSoup(self, resource):
        if resource == self.last_url:
            return self.last_soup

        pwb.output(f"<{resource=}>")
        response = None
        retry_limit = 20
        retries = 0
        while retries < retry_limit:
            try:

                # Throttle requests to one per second
                if (
                    "always" in self.options and self.options["always"]
                ):  # Waits enough when interactive
                    wait_time = 1.0 + self.lastRequest - time()
                    if wait_time > 0:
                        pwb.output(f"Sleeping {wait_time}s. (max 1 req. / s.)")
                        sleep(wait_time)
                    self.lastRequest = time()

                with requests.get(
                    url=f"https://www.treccani.it{resource}",
                    headers={
                        "User-Agent": "william-avery-bot",
                        "From": "tools.william-avery-bot@tools.wmflabs.org",
                    },
                    timeout=10,
                ) as resp:
                    if resp.status_code != 200:
                        pwb.output(
                            f"Treccani responded {resp.status_code} on {resource}"
                        )
                        if resp.status_code not in [408, 409, 429]:
                            break
                    else:
                        response = BeautifulSoup(resp.content, features="lxml")
                        self.last_url = resource
                        self.last_soup = response
                        break
            except (requests.exceptions.RequestException) as exc:
                pwb.output(repr(exc))

            retries += 1
            interval = min([retries * 2, 60])
            pwb.output(f"Retrying Treccani website in {interval}s. Retry {retries}")
            sleep(interval)
        return response

    def find_best_claim(self, claims):
            """Find the first best ranked claim.""" 
            if not claims:
                return None       
            #from pywikibot.page
            index = None
            for i, claim in enumerate(claims):
                if claim.rank == 'preferred':
                    return claim
                if index is None and claim.rank == 'normal':
                    index = i
            if index is None:
                index = 0
            return claims[index]

    def todayStamp(self) -> WbTime:
        """Return timestamp set to today's date"""
        return WbTime.fromTimestamp(
            Timestamp.combine(Timestamp.today(), Timestamp.min.time()), precision="day"
        )

    def yearStamp(self, year) -> WbTime:
        """Return timestamp representing the given year"""
        return WbTime.fromTimestamp(Timestamp(year, 1, 1), precision="year")

    def newClaim(self, prop, val, **kwargs) -> pwb.page.Claim:
        """Returns a new claim set to the given value"""
        clm = PropertyPage(self.site, prop).newClaim(**kwargs)
        clm.setTarget(val)
        return clm

    def newRef(self, prop, val) -> dict:
        clm = self.newClaim(prop, ItemPage(self.site, val), is_reference=True)
        return {prop: [clm], "P813": [self.currDateRef("P813")]}

    def currDateRef(self, prop) -> pwb.page.Claim:
        """Create a reference with today's date as value"""
        return self.newClaim(prop, self.todayStamp(), is_reference=True)

    def setSingleQual(self, clm, prop, val):
        """
        Set a single qualifier for the given property on a claim, with the given value,
        if it is not already set thus
        """
        if prop in clm.qualifiers:
            if len(clm.qualifiers[prop]) == 1 and clm.has_qualifier(prop, val):
                return
            del clm.qualifiers[prop]
        pwb.output(f"!{prop} set to '{val}'")
        clm.qualifiers[prop] = [self.newClaim(prop, val, is_qualifier=True)]

    def content_sample(self, soup):
        """
        If script is being run in interactive mode, output a snippet of the html content for a given URL
        """
        if not ("always" in self.options and self.options["always"]):
            try:
                pwb.output(
                    soup.select("div.module-article-full_content")[0]
                    .get_text()
                    .strip()[:120]
                )
            except Exception as exc:
                pwb.output(exc)

    def get_labels(self, langs) -> str:
        """A string of labels for languages in the given array of language codes"""
        return " / ".join(
            [
                f"{lang}:{self.current_page.labels[lang]}"
                for lang in langs
                if lang in self.current_page.labels
            ]
        )

    def id_is_used(self, prop, val) -> str:
        if items:=sparql.SparqlQuery().get_items(result_type=list, query=
        f"""SELECT DISTINCT ?item WHERE {{
            ?item p:{prop} ?statement0.
            ?statement0 (ps:{prop}) "{val}".
        }} LIMIT 1"""):
            pwb.output(items)
            return items[0]
        else:
            return None

    # Decorator will call db proc to mark item as processed, if option set
    @pagegenerators_ext.batchcontrol
    def treat_page(self) -> None:
        pwb.output(self.get_labels(["en", "it"]))
        curPage=self.current_page
        start=getattr(self.current_page, '_content', None)
        treccani_url_frag = {
            "P3365": "/enciclopedia/{}/",
            "P4223": "/enciclopedia/{}_(Enciclopedia-Italiana)/",
            "P6404": "/enciclopedia/{}_(Dizionario-di-Storia)/",
            "P1986": "/enciclopedia/{}_(Dizionario-Biografico)/",
            "P7993": "/enciclopedia/{}_(Dizionario-di-filosofia)/",
        }
        # Appropriate value of 'stated in' to use in refs for each property
        stated_in_map = {
            "P3365": "Q65921422",
            "P4223": "Q731361",
            "P6404": "Q84087675",
            "P1986": "Q1128537",
            "P7993": "Q3998195",
        }
        initial_prop = None  # An existing Treccani ID prop on the item
        existing_keys=[]
        new_claim_count=0
        for prop in treccani_url_frag.keys():
            if prop in curPage.claims:
                treccani_id_props = curPage.claims.get(prop)
                if not treccani_id_props or len(treccani_id_props) < 1:
                    pwb.warn(
                        f"Expecting at least one occurrence of Treccani property {prop}"
                    )
                else:
                    for propertyinst in treccani_id_props:
                        if (id := propertyinst.getTarget()):
                            existing_keys.append(id)
                            if not initial_prop:
                                initial_prop = prop
                        else:
                            pwb.warn(f"Found a falsy value on Treccani property {prop}")

        if not initial_prop:
            pwb.warn(f"No Treccani properties found")
            return

        treccani_id_props = curPage.claims.get(initial_prop)

        if (init_id := treccani_id_props[0].getTarget()) is False:
            pwb.warn(f"Found a falsy value on Treccani property {initial_prop}")
            return

        target_url = treccani_url_frag[initial_prop].format(init_id)
        if not (soup := self.treccaniSoup(target_url)):
            pwb.warn(f"Nothing at URL {target_url}")
            return

        self.content_sample(soup)
        latest_prop_set = (
            None  # Updated to appropriate property for each url to be processed
        )
        ref_added=False
        anchors = [treccani_url_frag[initial_prop].format(init_id)]
        for anchor in soup.select(
            "div.module-widget-more_results div.widget-nobg ul li a"
        ):
            # URL as it would appear in browser bar
            anchors.append(parse.unquote(anchor["href"]))

        for url in anchors:
            pwb.output(url)

            pub_year = None
            rmatch = re.match(
                "^/enciclopedia/(?P<id>([a-z0-9]+(\-[a-z0-9]+)*(_res\-[0-9a-f]{8}(\-[0-9a-f]{4}){3}\-[0-9a-f]{12})?))(_\((?P<op>(Enciclopedia-Italiana|Dizionario-di-Storia|Dizionario-Biografico|Dizionario-di-filosofia))\))?/$",
                url,
            )
            if not rmatch:
                continue

            pwb.output(f">>>>>>>id={rmatch['id']}")
            pwb.output(rmatch["op"])
            opstr = rmatch["op"].replace("-", " ") if rmatch["op"] else "~"

            if opstr == "Enciclopedia Italiana":
                curr_prop = "P4223"
            elif opstr == "Dizionario di Storia":
                curr_prop = "P6404"
            elif opstr == "Dizionario Biografico":
                opstr = "Dizionario Biografico degli Italiani"
                curr_prop = "P1986"
            elif opstr == "Dizionario di filosofia":
                curr_prop = "P7993"
            else:
                curr_prop = "P3365"

            if curr_prop not in curPage.claims:
                curPage.claims[curr_prop] = [] #ClaimCollection(self.site.data_repository())
            latest_prop_set = curPage.claims[curr_prop]

            pageSoup = self.treccaniSoup(url)
            id = rmatch["id"].strip()
            self.content_sample(pageSoup)

            # Use existing claim with this ID value, if present
            if currClaims := list(
                filter(lambda clm: clm.target_equals(id), latest_prop_set)
            ):
                currClaim = currClaims[0]
            else:
                if used_on_item:=self.id_is_used(curr_prop, id):
                    pwb.warn(f"id {id} present at property {curr_prop} on {used_on_item}")
                # Otherwise create a new one
                if not ("always" in self.options and self.options["always"]):
                    create_claim=pwb.input_choice(
                        f"!Create new claim {curr_prop} set to '{id}'",
                        [("Yes", "Y"), ("No", "n")],
                        "Y",
                    )
                    if create_claim != "y":
                        continue
                else:
                    # If automatically added, there must be a match between the IDs
                    # 
                    if not list(
                        filter(lambda existing_id:
                                    id==existing_id 
                                    # appendici
                                    or curr_prop=="P4223" and (id.startswith(existing_id) or existing_id.startswith(id)),
                                existing_keys)
                    ):
                        pwb.warn(f"No existing value matches {id} on {curr_prop}")
                        continue
                    if used_on_item:
                        continue
                pwb.output(f"!New claim {curr_prop} set to '{id}'")
                new_claim_count+=1
                currClaim = self.newClaim(curr_prop, id)
                

            # Most useful information is in the breadcrumb
            publicationMatch = None
            parts=[]
            if briciole_div := pageSoup.select_one(
                "div.module-briciole_di_pane div.treccani-container"
            ):
                briciole = re.sub(r"\s+", " ", briciole_div.contents[0])
                pwb.output(f"{briciole=}")
                parts = briciole.split(opstr, 1)
                if len(parts) > 1:
                    publicationMatch = re.match(
                        r"(?P<app>((I{1,3}|IV|V|VI|VII|IIX|IX|X) Appendice)|(Volume \d\d?))?\s*\((?P<year>\d{4})\)",
                        parts[1].strip(" -"),
                    )

            # Year of publication
            if "P577" not in currClaim.qualifiers:
                if publicationMatch and publicationMatch["year"]:
                    pwb.output(publicationMatch["year"])
                    pub_year = self.yearStamp(int(publicationMatch["year"]))
                    self.setSingleQual(currClaim, "P577", pub_year)

            # Authors
            # Leave existing values alone
            # Don't set author string if authors have been added as structured data, P50
            if (
                "P50" not in currClaim.qualifiers
                and "P2093" not in currClaim.qualifiers
                and parts
            ):
                aut = parts[0].rstrip(" -").strip()
                pwb.output(f"Authors: {aut=}")
                if autmatch := re.match(r"^di (?P<aut>.+)", aut):
                    # Many values have superfluous pairs of dash and asterisk
                    autstr = re.sub(r"- \*", "", autmatch["aut"])
                    # Web text uses hyphens as separators, plus "e" meaning "and"
                    autstr = re.sub(r" [-e] ", ", ", autstr.strip())
                    # Put all-cap surnames into title case
                    autstr = re.sub(
                        r"[A-Z]+",  # This works for D'Acampo, O'Connor, etc
                        lambda name: name.group(0).capitalize(),
                        autstr,
                    )
                    self.setSingleQual(currClaim, "P2093", autstr)

            # Stated as
            if statedAs := pageSoup.select_one("h1.title-search"):
                self.setSingleQual(currClaim, "P1810", statedAs.contents[0].strip())

            # Appendice or volume
            if publicationMatch and publicationMatch["app"]:
                pwb.output(f"Vol/Appendice={publicationMatch['app']}")
                app = publicationMatch["app"].removeprefix("Volume ")
                self.setSingleQual(currClaim, "P478", app.strip())

            # Check there is an appropriate 'stated in' reference on the claim
            currRef = None
            for ref in currClaim.sources:
                if "P248" in ref and ref["P248"][0].target_equals(
                    stated_in_map[curr_prop]
                ):
                    currRef = ref
                    # Add a 'retrieved date' of today to existing reference, if none present
                    if "P813" not in ref:
                        pwb.output(f"!New P813 on {curr_prop} ref")
                        ref["P813"] = [self.currDateRef("P813")]
                        ref_added=True

            # Add a 'stated in' ref, if none was found
            if not currRef:
                pwb.output(f"!New ref on  {curr_prop}")
                currClaim.sources.append(self.newRef("P248", stated_in_map[curr_prop]))
                ref_added=True

            if not currClaim.snak: # new Claim
                dob_claims= curPage.claims.get('P569') # Date of birth
                dob_claim=self.find_best_claim(dob_claims)
                if dob_claim and pub_year:
                    dob=dob_claim.getTarget()
                    if hasattr(dob, 'year')  and dob.year > 1:
                        pub_diff=pub_year.toTimestamp() - dob.toTimestamp()
                        if pub_diff.days < 0:
                            pwb.warn(f"Date of birth after publication date: {pub_diff} days")
                            continue
                curPage.claims[curr_prop].insert(0, currClaim)
                if (self.options['report_page']):

                    new_prop_page = PropertyPage(self.site, curr_prop)
                    format_url_val = new_prop_page.claims.get('P1630')[0].getTarget()
                    new_url = format_url_val.replace("$1", id)
                    init_prop_page = PropertyPage(self.site, initial_prop)
                    init_format_url_val = init_prop_page.claims.get('P1630')[0].getTarget()
                    init_url = init_format_url_val.replace("$1", init_id)
                    output=f"""
| [[{curPage.title()}#{curr_prop}]]
| {self.get_labels(['en', 'it'])}
| [{init_url} {init_id}]
| [{new_url} {id}]
|-
"""
                    report_page = Page(pwb.Site(), self.options['report_page'])
                    if not report_page.text:
                        report_page.text = """
==Treccani ID claims added by William Avery Bot==
{|
! style="text-align:left;"| QID
! style="text-align:left;"| Title
! style="text-align:left;"| Existing
! style="text-align:left;"| New
|-
<!--NEWENTRIES-->
|}"""
                    report_page.text=report_page.text.replace("<!--NEWENTRIES-->\n", "<!--NEWENTRIES-->\n" + output )
                    report_page.save(f"[[{curPage.title()}]] {curr_prop}")

        if not (latest_prop_set or ref_added):  # No suitable urls were found and processed
            return
        #self.current_page.claims.set_on_item(self.current_page)
        # check for changes
        if not self.current_page.toJSON(
            diffto=getattr(self.current_page, "_content", None)
        ):
            return
        try:
            summary = "Import and enhance Treccani IDs"
            if new_claim_count:
                summary += f" (+{new_claim_count})"
            # 'Always update' option is assumed to indicate bot operation
            if "always" in self.options and self.options["always"]:
                summary += ". See [[Wikidata:Requests_for_permissions/Bot/William_Avery_Bot_3|request for permission]]"
                self.current_page.editEntity(summary=summary)
            else:
                self.user_edit_entity(self.current_page, summary=summary)
        except(pwb.exceptions.OtherPageSaveError) as exc:
            pwb.output(repr(exc))


def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {'report_page': None}

    local_args = pwb.handle_args(args)

    gen_factory = pagegenerators_ext.GeneratorFactory()

    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(":")
        option = arg[1:]
        if option in ('report_page'):
            if not value:
                pywikibot.input('Please enter a value for ' + arg)
            options[option] = value
        else:
            options[option] = True

    gen = gen_factory.getCombinedGenerator(preload=True)

    if gen:
        options["generator"] = gen
        bot = treccaniScraper(**options)
        bot.run()
    else:
        pwb.bot.suggest_help(missing_generator=True)


if __name__ == "__main__":
    main()
