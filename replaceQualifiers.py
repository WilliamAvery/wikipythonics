#!/usr/bin/python3
"""
One-off Wikidata script to change qualifiers on values of family name (P734)
from Criterion Used (P1013) to Object Has Role (P3831).
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2022
# (C) William Avery, 2022
#
# Distributed under the terms of the MIT license.
#
import pywikibot
from pywikibot import PropertyPage
from pywikibot.bot import (
    ExistingPageBot,
    SingleSiteBot,
    WikidataBot
)
import pagegenerators_ext as pagegenerators

class ReplaceQuals(
    WikidataBot
):
    def __init__(self, **kwargs):
        super(ReplaceQuals, self).__init__(**kwargs)

        self.options = kwargs    

        self.FAMILY_NAME='P734'
        self.CRITERION_USED='P1013'
        self.OBJECT_HAS_ROLE='P3831'

    def treat_page(self) -> None:
        replaced_count = 0
        if self.FAMILY_NAME not in self.current_page.claims:
            pywikibot.warning(f"No claim found for {self.FAMILY_NAME}")
            return

        for claim in self.current_page.claims[self.FAMILY_NAME]:
            if self.CRITERION_USED not in claim.qualifiers:
                continue

            if self.OBJECT_HAS_ROLE in claim.qualifiers:
                pywikibot.warning(f"Qualifier {self.OBJECT_HAS_ROLE} already present")
                continue
            qual_array=[]

            for qual_claim in reversed(claim.qualifiers[self.CRITERION_USED]):
                if qual_claim.target_equals('Q1376230') or qual_claim.target_equals('Q30232378'):
                    newQual = PropertyPage(self.site, self.OBJECT_HAS_ROLE).newClaim(is_qualifier=True)
                    newQual.setTarget(qual_claim.getTarget())
                    qual_array.append(newQual)
                    claim.qualifiers[self.CRITERION_USED].remove(qual_claim)
                    replaced_count+=1
            if qual_array:
                    claim.qualifiers[self.OBJECT_HAS_ROLE]=qual_array

        if not replaced_count: return
     
        summary = f"Apply standardised qualifiers to values of family name (×{replaced_count})"
        # 'Always update' option is assumed to indicate bot operation
        if "always" in self.options and self.options["always"]:
            summary += ". See [[Wikidata:Requests_for_permissions/Bot/William_Avery_Bot_8|request for permission]]"
            self.current_page.editEntity(summary=summary)
        else:
            self.user_edit_entity(self.current_page, summary=summary)

def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {}
    # Process global arguments to determine desired site
    local_args = pywikibot.handle_args(args)

    # This factory is responsible for processing command line arguments
    # that are also used by other scripts and that determine on which pages
    # to work on.
    gen_factory = pagegenerators.GeneratorFactory()

    # Process pagegenerators arguments
    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(":")
        if arg[0] != '-':
            pywikibot.output(f"Error on '{arg}' - Command line options start with hyphen")
            raise SystemExit(1)
        option = arg[1:]
        if option == 'always':
            if value:
                pywikibot.output(f"Command line option '{arg}' takes no value")
                raise SystemExit(1)
            options[option] = True
        else:
            pywikibot.output(f"Did not understand command line option '{arg}'")
            raise SystemExit(1)

    # The preloading option is responsible for downloading multiple
    # pages from the wiki simultaneously.
    gen = gen_factory.getCombinedGenerator(preload=True)

    # check if further help is needed
    if not pywikibot.bot.suggest_help(missing_generator=not gen):
        # pass generator and private options to the bot
        bot = ReplaceQuals(generator=gen, **options)
        bot.run()
''

if __name__ == '__main__':
    main()