#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Add sitelinks on wikidata to enwiki pages, depending on taxonbar parameter value

The following generators and filters are supported:

&params;
"""
#
# (C) William Avery 2020, based on basic.py
# Distributed under the terms of the MIT license.
#
import sys

import pywikibot
from pywikibot import pagegenerators
from datetime import timedelta
from pywikibot.bot import (
    ExistingPageBot, NoRedirectPageBot)
import mwparserfromhell
# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {'&params;': pagegenerators.parameterHelp}  # noqa: N816


class taxonbarSyncer(
    ExistingPageBot,
    NoRedirectPageBot
):
    def __init__(self, generator, runstarttime, **kwargs):
        self.availableOptions.update({
            'agelimit': None,
        })

        super(taxonbarSyncer, self).__init__(**kwargs)

        self.generator = generator
        self.datasite = pywikibot.Site('wikidata', 'wikidata')
        self.runstarttime = runstarttime
        self.agelimit = None 
        if ("agelimit" in kwargs):
            self.agelimit = timedelta(hours=kwargs["agelimit"])
            pywikibot.output("agelimit = {}".format(str(self.agelimit)))
    def treat_page(self):
        page_t = self.current_page.title()
        age=self.runstarttime - self.current_page.editTime()
        pywikibot.output('{} is {} old'.format(page_t, str(age)))
        if (self.agelimit is None or age < self.agelimit):
            entity = self.datasite.loadcontent({'sites': 'enwiki',
                                            'titles': page_t})
            entity_keys = list(entity.keys())
            if (entity_keys[0] != "-1"):
                pywikibot.output(page_t + ' already linked to ' + entity_keys[0])
            else:
                qidparam = None
                qidvalue = ''
                mwpfh = mwparserfromhell.parse(self.current_page.text)
                for template in mwpfh.filter_templates():
                    if template.name.matches(["Taxobar", "Taxonbar"]):
                        try:
                            qidparam = template.get("from")
                        except ValueError:
                            pass
                        if (qidparam is None):
                           try:
                               qidparam = template.get("from1")
                           except ValueError:
                               pass
                if not qidparam is None:
                    qidvalue = str(qidparam.value)

                pywikibot.output('QID for {} = {}'.format(page_t, qidvalue))

                if qidvalue:
                    res = pywikibot.ItemPage(self.datasite, qidvalue)
                    #linktarget = res[qidvalue]
                    data = res.get()
                    pywikibot.output(list(data))
                    if res:
                        if "enwiki" in data["sitelinks"]:
                            pywikibot.output('QID {} linked to {}'.format(
                                qidvalue, data["sitelinks"]["enwiki"].title))
                        else:
                            res.setSitelink(sitelink={'site': 'enwiki', 'title': page_t},
                                        summary=u'Set enwiki sitelink from enwiki taxonbar parameter on {}'.format(page_t))
                    else:
                        pywikibot.output('QID {} not found'.format(qidvalue))


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    options = {}
    # Process global arguments to determine desired site
    local_args = pywikibot.handle_args(args)

    gen_factory = pagegenerators.GeneratorFactory()

    # Parse command line arguments
    for arg in local_args:

        # Now pick up your own options
        option, sep, value = arg.partition(':')
        if option == "-agelimit":
            options['agelimit'] = int(value)
            continue
        # Catch the pagegenerators options
        if gen_factory.handleArg(arg):
            continue  # nothing to do here


    # The preloading option is responsible for downloading multiple
    # pages from the wiki simultaneously.
    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        bot = taxonbarSyncer(gen, gen_factory.site.server_time(), **options)
        bot.run()
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == '__main__':
    main()
