"""
Add a few options to the standard list of pywikibot generators
This is a module to be imported, not a script to be run 
"""
#
# (C) William Avery 2021
#
# Distributed under the terms of the MIT license.
#
import pywikibot
from pywikibot import pagegenerators
from collections.abc import Generator
from pywikibot.data import mysql
import mysql_ext

from typing import Any, Optional, Union
from pywikibot.backports import Iterator

OPT_SITE_TYPE = Optional['pywikibot.site.BaseSite']

def MySQLPageIdGenerator(query, dbname):
    def gen(query, dbname): 
        for row in mysql.mysql_query(query, dbname=dbname):
            yield row[0]
    return pagegenerators.PagesFromPageidGenerator(
        gen(query, dbname)
    )


def MySQLItemBatch(query, dbname, posttreatproc, preload=True):

    return GeneratorFactory().getCombinedGenerator(
        gen=MySQLItemGenerator(
            query,
            dbname=dbname,
        ),
        mysqldb=dbname,
        posttreatproc=posttreatproc,
        preload=preload,
    )

def MySQLItemGenerator(query: str, dbname: str = None, 
                       site: OPT_SITE_TYPE = None,
                       posttreatproc: str = None,
                       verbose: Optional[bool] = None
                       ) -> Iterator['pywikibot.page.ItemPage']:
    """
    Yield a list of item pages based on a MySQL query.

    The query should return two columns, page namespace and page title pairs
    from some table. An example query that yields all ns0 pages might look
    like::

        SELECT
         page_namespace,
         page_title
        FROM page
        WHERE page_namespace = 0;

    .. seealso:: :manpage:`MySQL`

    :param query: MySQL query to execute
    :param site: Site object
    :param verbose: if True, print query to be executed;
        if None, config.verbose_output will be used.
    :return: generator which yields pywikibot.Page
    """
    from pywikibot.data import mysql

    if site is None:
        site = (pywikibot.Site()).data_repository()
    if dbname is None:
        dbname = site.dbName()

    for row in mysql.mysql_query(query, dbname=dbname):
        page_name = row[0]  # .decode(self.site.encoding())
        page = site.data_repository().get_entity_for_entity_id(page_name)
        yield page 

def batchcontrol(treat):
    """Invoked as a decorator on treat. 
    Marks an item as processed, if treat returns false value"""

    def inner(bot):
        if (
            treat(bot)
            and hasattr(bot, "generator")
            and hasattr(bot.generator, "posttreatproc")
            and hasattr(bot.generator, "mysqldb")
            and bot.generator.posttreatproc
            and bot.generator.mysqldb
        ):
            pywikibot.output(
                f"using '{bot.generator.posttreatproc}' for {bot.current_page.getID()}"
            )
            if isinstance(bot.current_page, pywikibot.ItemPage):
                param=bot.current_page.getID().encode() # Q#
            else:
                param=bot.current_page.pageid

            mysql_ext.mysql_procedure(
                bot.generator.posttreatproc,
                params=[param],
                dbname=bot.generator.mysqldb,
            )

    return inner


# Object supplied to bot as generator will be overwritten unless instance_of collections.abc.Generator
@Generator.register
class generator_ext:
    """Dummyish implementation, but required not to be nixed"""

    def __init__(self, gen, posttreatproc, mysqldb):
        self.__gen__ = gen
        self.posttreatproc = posttreatproc
        self.mysqldb = mysqldb

    def close(self):
        pass

    def send(self, value):
        pass

    def throw(self):
        pass

    def __iter__(self):
        for each in self.__gen__:
            yield each


class GeneratorFactory(pagegenerators.GeneratorFactory):
    """Extends the standard GeneratorFactory with methods to handle additional arguments"""

    def __init__(self, **kwargs):

        super(GeneratorFactory, self).__init__(**kwargs)

        self.mysqldb = None  # Can override use of the site's replica db
        self.posttreatproc = None  # db proc to run after item has been treated

    def _handle_item(self, value):
        """Handle `-item` argument to return a Wikidata entity."""
        if not value:
            value = pywikibot.input("What item do you want to use?")
        return [self.site.data_repository().get_entity_for_entity_id(value)]

    def _handle_mysqldb(self, value):
        """Handle `mysqldb` argument, to use a db that is not a replica."""
        self.mysqldb = value
        return True

    def _handle_mysqlpageids(self, value):
        """A mysql query that generates pageids"""

        dbname = self.mysqldb if self.mysqldb else pywikibot.Site().dbName()
        return MySQLPageIdGenerator(value, dbname=dbname)

    def _handle_posttreatproc(self, value):
        """Handle `posttreatproc` argument, to call a dbproc after treating page."""
        self.posttreatproc = value
        return True

    def _handle_mysqlquery_item(self, value):
        """Handle `-mysqlquery_item` argument. Returns items using a mysql query"""
        if not value:
            value = pywikibot.input("Mysql query string:")

        dbname = self.mysqldb if self.mysqldb else pywikibot.Site().dbName()

        return MySQLItemGenerator(value, dbname=dbname)

    def getCombinedGenerator(self, **kwargs):
        """Injects some extra arguments to handle batches of QIDs from non-replica db"""
        if 'gen' in kwargs:
            if hasattr(kwargs['gen'], 'mysqldb'):
                self.mysqldb = getattr(kwargs['gen'], 'mysqldb')
            if hasattr(kwargs['gen'], 'postreatproc'):
                self.mysqldb = getattr(kwargs['gen'], 'posttreatproc')
        if 'mysqldb' in kwargs:
            self.mysqldb = kwargs.pop('mysqldb')
        if 'posttreatproc' in kwargs:
            self.posttreatproc = kwargs.pop('posttreatproc')  

        if gen:= super(GeneratorFactory, self).getCombinedGenerator(**kwargs):
            return generator_ext(
                gen,
                self.posttreatproc,
                self.mysqldb,
            )
        else:
            return gen