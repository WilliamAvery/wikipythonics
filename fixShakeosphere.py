#!/usr/bin/python
"""
One-off Wikidata script to shift Shakeosphere IDs by 24638 and test if the new id looks correct
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2022
# (C) William Avery, 2022
#
# Distributed under the terms of the MIT license.
#

import re
import json
import requests
from urllib import parse
from bs4 import BeautifulSoup
import pywikibot
from datetime import datetime
from time import sleep, time
from pywikibot import Timestamp, WbTime, pagegenerators, Page, ItemPage, PropertyPage
import pagegenerators_ext

from pywikibot.bot import WikidataBot

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {"&params;": pagegenerators.parameterHelp}


class FixShakeosphere(
    WikidataBot,  # Only used to get user_edit_entity method for testing, treat_page is overridden
):
    def __init__(self, **kwargs):
        self.available_options.update(
            {
                "report_page": None,  # name of a page to log results of matching
                "update": None,  # specify to carry out update processing
            }
        )

        super(FixShakeosphere, self).__init__(**kwargs)

        self.options = kwargs
        self.lastRequest = 0
        
        self.stated_in_prop = "P248"
        self.retrieved_prop = "P813"
        self.retrieve_cutoff = datetime(2022, 6, 10)
        self.named_as_prop = "P1810"
        self.shakeosphere_id_prop = "P2886"

        self.stated_in_val = "Q24284201"
        self.matches = 0

    def teardown(self):
        print(self.matches)

    def shakeo_soup(self, resource):

        # pywikibot.output(f"<{resource=}>")
        response = None
        retry_limit = 20
        retries = 0
        while retries < retry_limit:
            try:

                # Throttle requests to one per second
                if (
                    "always" in self.options and self.options["always"]
                ):  # Waits enough when interactive
                    wait_time = 1.0 + self.lastRequest - time()
                    if wait_time > 0:
                        pywikibot.output(f"Sleeping {wait_time}s. (max 1 req. / s.)")
                        sleep(wait_time)
                    self.lastRequest = time()

                with requests.get(
                    url=f"https://shakeosphere.lib.uiowa.edu/persons/person.jsp?pid={resource}",
                    headers={
                        "User-Agent": "william-avery-bot",
                        "From": "tools.william-avery-bot@tools.wmflabs.org",
                    },
                    timeout=10,
                ) as resp:
                    if resp.status_code != 200:
                        pywikibot.output(
                            f"Treccani responded {resp.status_code} on {resource}"
                        )
                        if resp.status_code not in [408, 409, 429]:
                            break
                    else:
                        response = BeautifulSoup(resp.content, features="lxml")
                        break
            except (requests.exceptions.RequestException) as exc:
                pywikibot.output(repr(exc))

            retries += 1
            interval = min([retries * 2, 60])
            pywikibot.output(
                f"Retrying Shakeosphere website in {interval}s. Retry {retries}"
            )
            sleep(interval)
        return response

    def find_best_claim(self, claims):
        """Find the first best ranked claim."""
        if not claims:
            return None
        # from pywikibot.page
        index = None
        for i, claim in enumerate(claims):
            if claim.rank == "preferred":
                return claim
            if index is None and claim.rank == "normal":
                index = i
        if index is None:
            index = 0
        return claims[index]

    def match_label(self, stated_as):
        retval = {"lang": None, "exact": None, "alias": None, "match_str": ""}
        for lang in self.current_page.labels:
            if self.current_page.labels[lang] == stated_as:
                retval["lang"] = lang
                retval["exact"] = True
                retval["match_str"] = stated_as
                return retval

        for lang in self.current_page.aliases:
            if stated_as in self.current_page.aliases[lang]:
                retval["lang"] = lang
                retval["exact"] = True
                retval["alias"] = True
                retval["match_str"] = stated_as
                return retval

        for lang in self.current_page.labels:
            if self.current_page.labels[lang].startswith(
                stated_as
            ) or stated_as.startswith(self.current_page.labels[lang]):
                retval["lang"] = lang
                retval["exact"] = False
                retval["match_str"] = self.current_page.labels[lang]
                return retval

        for lang in self.current_page.aliases:
            print(self.current_page.aliases[lang])
            for alias in self.current_page.aliases[lang]:
                if alias.startswith(stated_as) or stated_as.startswith(alias):
                    retval["lang"] = lang
                    retval["exact"] = False
                    retval["alias"] = True
                    retval["match_str"] = alias
                    return retval

        print(self.current_page.labels)
        print(self.current_page.aliases)
        return retval

    def todayStamp(self) -> WbTime:
        """Return timestamp set to today's date"""
        return WbTime.fromTimestamp(
            Timestamp.combine(Timestamp.today(), Timestamp.min.time()), precision="day"
        )

    def new_claim(self, prop, val, **kwargs) -> pywikibot.page.Claim:
        """Returns a new claim set to the given value"""
        clm = PropertyPage(self.site, prop).newClaim(**kwargs)
        clm.setTarget(val)
        return clm

    def newSource(self, named_as) -> dict:
        return {
            self.named_as_prop: [
                self.new_claim(self.named_as_prop, named_as, is_reference=True)
            ],
            self.stated_in_prop: [
                self.new_claim(
                    self.stated_in_prop,
                    ItemPage(self.site, "Q24284201"),
                    is_reference=True,
                )
            ],
            self.retrieved_prop: [
                self.new_claim(
                    self.retrieved_prop, self.todayStamp(), is_reference=True
                )
            ],
        }

    def treat_page(self) -> None:

        p_item = self.current_page

        add_count = 0
        changed_count = 0
        idclaim = None

        if self.shakeosphere_id_prop not in self.current_page.claims:
            pywikibot.warn(f"No claim found for {self.shakeosphere_id_prop}")
            return
        idclaims = self.current_page.claims[self.shakeosphere_id_prop]
        idclaim = self.find_best_claim(idclaims)

        print(idclaim.sources)
        for source in idclaim.sources:
            if self.retrieved_prop in source:
                retrieved = source[self.retrieved_prop][0].getTarget()
                if retrieved:
                    retrieved_ts = retrieved.toTimestamp()

                    if retrieved_ts >= self.retrieve_cutoff:
                        print(
                            f"Retrieved Date diff: {retrieved_ts - self.retrieve_cutoff}"
                        )
                        return

        id = idclaim.getTarget()
        newid = str(int(id) + 24638)
        response = self.shakeo_soup(newid)
        # print(response)
        stated_as_text = "<NOT FOUND>"
        stated_as_elem = response.select_one("div#center > h2")
        if stated_as_elem and len(stated_as_elem):
            stated_as_text = str(stated_as_elem.contents[0]).strip()
        print(stated_as_text)
        match_res = self.match_label(stated_as_text)
        print(match_res)
        if match_res["match_str"]:
            self.matches += 1

        if self.options["report_page"]:

            if stated_as_text == "<NOT FOUND>":
                color = "purple"
            elif match_res["exact"]:
                color = "lightgreen"
            elif match_res["match_str"]:
                color = "orange"
            else:
                color = "red"
            output = f"""
| [[{self.current_page.title()}]]
| {self.current_page.labels['en'] if 'en' in self.current_page.labels else '' }
| style="text-align:left; background-color:{color}"| {match_res["match_str"]}
| {stated_as_text}
|-
"""
            report_page = Page(pywikibot.Site(), self.options["report_page"])
            if not report_page.text:
                report_page.text = """
==Report on Shakeosphere Person IDs William Avery Bot==
{|
! style="text-align:left;"| QID
! style="text-align:left;"| Label
! style="text-align:left;"| Match
! style="text-align:left;"| Stated as
|-
<!--NEWENTRIES-->
|}"""
            report_page.text = report_page.text.replace(
                "<!--NEWENTRIES-->\n", "<!--NEWENTRIES-->\n" + output
            )
            report_page.save(
                f"[[{self.current_page.title()}]] {match_res['match_str']}"
            )

        if stated_as_text == "<NOT FOUND>":
            return
        if not self.options["update"]:
            return

        idclaim.sources = [self.newSource(stated_as_text)]  # Completely new sources
        idclaim.setTarget(newid)

        summary = f"New value of Shakeosphere person ID (P2886)"

        # 'Always update' option is assumed to indicate bot operation
        if "always" in self.options and self.options["always"]:
            summary += ". See [[Wikidata:Requests_for_permissions/Bot/William_Avery_Bot_6|request for permission]]"
            self.current_page.editEntity(summary=summary)
        else:
            self.user_edit_entity(self.current_page, summary=summary)


def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {"report_page": None, "update": False}

    local_args = pywikibot.handle_args(args)

    gen_factory = pagegenerators_ext.GeneratorFactory()

    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(":")
        option = arg[1:]
        if option in ("report_page"):
            if not value:
                pywikibot.input("Please enter a value for " + arg)
            options[option] = value
        else:
            options[option] = True

    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        options["generator"] = gen
        bot = FixShakeosphere(**options)
        bot.run()
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == "__main__":
    main()
