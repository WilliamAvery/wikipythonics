"""
Simple mailer to send error messages from a Kubernetes container.
Intended for use if a bot process crashes or needs to send info.
For format of ~/mailer.config file see the sample, mailer.config
This is a module to be imported, not a script to be run 
"""
#
# (C) William Avery 2022
#
# Distributed under the terms of the MIT license.
#
import smtplib
import ast
from os.path import expanduser
from collections import namedtuple
import traceback
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class SendMailSession:
    def __init__(self, config="~/mailer.config"):

        with open(expanduser(config), "r") as conffile:
            conf = ast.literal_eval(conffile.read())
        self.smtpsession = smtplib.SMTP(**conf["smtp_server"])
        self.smtpsession.starttls()
        if 'credentials' in conf:
            self.smtpsession.login(**conf["credentials"])
        self.defaults = namedtuple("Defaults", conf["defaults"].keys())(
            *conf["defaults"].values()
        )
        del conf

    def __del__(self):
        try:
            self.smtpsession.quit()
        except:
            pass

    def send(self, subject, text, to=None, from_addr=None):

        if not to:
            to = self.defaults.to_address
        if not from_addr:
            from_addr = self.defaults.from_address

        mimemsg = MIMEMultipart()
        mimemsg["From"] = from_addr
        mimemsg["To"] = to
        mimemsg["Subject"] = subject

        mimemsg.attach(MIMEText(text, "plain"))

        self.smtpsession.sendmail(from_addr, to, mimemsg.as_string())

    def sendException(self, subject, excinfo, to=None, from_addr=None):
        self.send(subject, "".join(traceback.format_exception(*excinfo)), to, from_addr)
