#
# (C) William Avery, 2022
#
# Distributed under the terms of the MIT license.
#
import sys
import pymysql as mariadb
import pywikibot
from pywikibot.pagegenerators import SearchPageGenerator
print(sys.argv[1])
conn_batch = mariadb.connect(
    #host="localhost",
    #port=4711,
    host="tools.db.svc.wikimedia.cloud",
    database="s54354__batchcontrol",
    read_default_file="~/replica.my.cnf",
    autocommit=True
)
batchcur = conn_batch.cursor()

pywikibot.login.LoginManager()
gen = SearchPageGenerator(f"insource:plainrowheaders {sys.argv[1]}", namespaces=0)
for pge in gen:
    qry = f"INSERT INTO prh_batch (id, namespace, title, status) VALUES (%s, %s, %s, %s)  ON DUPLICATE KEY UPDATE status=0"
    args = [pge.pageid, (pge.namespace()).id, pge.title().encode(), pge.pageid]
    print(args)
    batchcur.execute(qry, args)
