#!/usr/bin/python3
"""
One-off Wikidata script to ensure IPSN URLs are as up-to-date as possible.

Requires a ~/LPSN.config file containing user credentials.

The LPSN database is licensed CC BY-NC 4.0, but Wikidata requires information
to be CC0. Do not modify this script to copy information into Wikidata wholesale.
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2022
# (C) William Avery, 2022
#
# Distributed under the terms of the MIT license.
#
import ast
import urllib
import os

from os.path import expanduser
import pywikibot
from pywikibot import WbTime, ItemPage, PropertyPage, Timestamp
from pywikibot.bot import ExistingPageBot, SingleSiteBot, WikidataBot
from lpsn import LpsnClient
import pagegenerators_ext as pagegenerators


class FixLPSNURLs(WikidataBot):
    def __init__(self, **kwargs):
        super(FixLPSNURLs, self).__init__(**kwargs)

        self.options = kwargs
        with open(expanduser("~") + "/LPSN.config", "r") as conf:
            self.client = LpsnClient(**ast.literal_eval(conf.read()))

    def todayStamp(self) -> WbTime:
        """Return timestamp set to today's date"""
        return WbTime.fromTimestamp(
            Timestamp.combine(Timestamp.today(), Timestamp.min.time()), precision="day"
        )

    def newClaim(self, prop, val, **kwargs) -> pywikibot.page.Claim:
        """Returns a new claim set to the given value"""
        clm = PropertyPage(self.site, prop).newClaim(**kwargs)
        clm.setTarget(val)
        return clm

    def newRef(self, prop, val) -> dict:
        clm = self.newClaim(prop, ItemPage(self.site, val), is_reference=True)
        return {prop: [clm], "P813": [self.currDateRef("P813")]}

    def currDateRef(self, prop) -> pywikibot.page.Claim:
        """Create a reference with today's date as value"""
        return self.newClaim(prop, self.todayStamp(), is_reference=True)

    # Decorator will call db proc to mark item as processed, if using batch control
    @pagegenerators.batchcontrol
    def treat_page(self) -> None:
        replaced_count = True
        if (
            "P1991" in self.current_page.claims
            and len(self.current_page.claims["P1991"]) > 1
        ):
            pywikibot.warn(
                "Item with more than one current LPSN URL requires manual intervention"
            )
            return
        if "P225" not in self.current_page.claims:
            pywikibot.warn("Item has no taxon name")
            return
        if plen := len(self.current_page.claims["P225"]) > 1:
            pywikibot.warn("Item more than one taxon name")
            return
        taxon_name = self.current_page.claims["P225"][0].getTarget()
        if "P105" not in self.current_page.claims:
            pywikibot.warn("Item has no taxon rank")
            return
        if plen := len(self.current_page.claims["P105"]) > 1:
            pywikibot.warn("Item more than one taxon rank")
            return
        if not (taxon_rank := self.current_page.claims["P105"][0].getTarget()):
            pywikibot.warn("Item has no taxon rank")
            return


        # the prepare method fetches all LPSN-IDs matching your query
        # and returns the number of IDs found

        print(f"name='{taxon_name}'")
        count = self.client.search(
            taxon_name=taxon_name, category=taxon_rank.labels["en"]
        )
        if not count:
            pywikibot.warn("No results returned from LPSN API")
            return
        taxon_names=(taxon_name, '"' + taxon_name + '"')
        # the retrieve method lets you iterate over all results
        # and returns the full entry as dict
        # Entries can be further filtered using a list of keys (e.g. ['keywords'])
        results = list(self.client.retrieve())
        print(results)
        filtered = list(filter(lambda res: res['full_name'] == taxon_name, results))
        print(filtered)
        if len(filtered) != 1:
            pywikibot.warn(f"{len(filtered)} results after filtering")
            return
        lpsn_address = filtered[0]['lpsn_address']
        action=""
        if "P1991" not in self.current_page.claims:
            self.current_page.claims["P1991"]=[self.newClaim("P1991", lpsn_address)]
            action="Add"
        elif self.current_page.claims["P1991"][0].getTarget() != lpsn_address:
            self.current_page.claims["P1991"][0]=self.newClaim("P1991", lpsn_address)
            action="Update"
        elif not self.current_page.claims["P1991"][0].sources:
            action="Verify"
        else:
            return

        self.current_page.claims["P1991"][0].sources=[self.newRef("P248", "Q6595107")]
        summary = f"{action} LPSN URL"
        # 'Always update' option is assumed to indicate bot operation
        if "always" in self.options and self.options["always"]:
            summary += ". [[Wikidata:Requests_for_permissions/Bot/William_Avery_Bot_10|Bot task 10]]"
            self.current_page.editEntity(summary=summary)
        else:
            self.user_edit_entity(self.current_page, summary=summary)


def main(*args: str) -> None:
    os.environ['TZ'] = 'CUT'    # Wikimedia uses CUT
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {}
    # Process global arguments to determine desired site
    local_args = pywikibot.handle_args(args)

    # This factory is responsible for processing command line arguments
    # that are also used by other scripts and that determine on which pages
    # to work on.
    gen_factory = pagegenerators.GeneratorFactory()

    # Process pagegenerators arguments
    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(":")
        if arg[0] != "-":
            pywikibot.output(
                f"Error on '{arg}' - Command line options start with hyphen"
            )
            raise SystemExit(1)
        option = arg[1:]
        if option == "always":
            if value:
                pywikibot.output(f"Command line option '{arg}' takes no value")
                raise SystemExit(1)
            options[option] = True
        else:
            pywikibot.output(f"Did not understand command line option '{arg}'")
            raise SystemExit(1)

    # The preloading option is responsible for downloading multiple
    # pages from the wiki simultaneously.
    gen = gen_factory.getCombinedGenerator(preload=True)

    # check if further help is needed
    if not pywikibot.bot.suggest_help(missing_generator=not gen):
        # pass generator and private options to the bot
        (FixLPSNURLs(generator=gen, **options)).run()


""

if __name__ == "__main__":
    main()
