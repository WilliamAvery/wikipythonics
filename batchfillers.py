#
# (C) William Avery, 2022
#
# Distributed under the terms of the MIT license.
#
import pymysql as mariadb
import pywikibot
def huntTrackers():
    conn_batch = mariadb.connect(
        host="tools.db.svc.wikimedia.cloud",
        database="s54354__batchcontrol",
        read_default_file="~/replica.my.cnf",
    )
    batchcur = conn_batch.cursor()

    conn_data = mariadb.connect(
        host="wikidatawiki.analytics.db.svc.wikimedia.cloud",
        database="wikidatawiki_p",
        read_default_file="~/replica.my.cnf",
    )
    datacur = conn_data.cursor()

    batchcur.execute("select last_rc_id from rc_tracker where proc_key='CLEANSEREFS'")
    last_ref = batchcur.fetchone()[0]
    pywikibot.output(f"Last max rc_id = {last_ref}")

    # Make allowance for table not getting filled in order of rc_id
    datacur.execute(f"select max(rc_id) from recentchanges where rc_id > {last_ref} and rc_timestamp < CAST(DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 HOUR),'%Y%m%d%H%i%s') AS INTEGER)")
    max_rc_id = datacur.fetchone()[0]

    if not max_rc_id:
        max_rc_id = last_ref
    pywikibot.output(f"Current max rc_id = {max_rc_id}")
    qrystr = f" \
    SELECT rc_title, el_to_path \
    FROM recentchanges   \
    JOIN externallinks on el_from=rc_cur_id and rc_namespace=0    \
    WHERE (\
    el_to_path like '%&ef\\_id=%' or el_to_path like '%?ef\\_id=%' \
    or el_to_path like '%&s\\_kwcid=%' or el_to_path like '%?s\\_kwcid=%' \
    or el_to_path like '%&utm\\_source=%' or el_to_path like '%?utm\\_source=%' \
    or el_to_path like '%&utm\\_medium=%' or el_to_path like '%?utm\\_medium=%' \
    or el_to_path like '%&utm\\_term=%' or el_to_path like '%?utm\\_term=%' \
    or el_to_path like '%&utm\\_campaign=%' or el_to_path like '%?utm\\_campaign=%' \
    or el_to_path like '%&utm\\_content=%' or el_to_path like '%?utm\\_content=%' \
    or el_to_path like '%&utm\\_cid=%' or el_to_path like '%?utm\\_cid=%' \
    or el_to_path like '%&utm\\_reader=%' or el_to_path like '%?utm\\_reader=%' \
    or el_to_path like '%&utm\\_referrer=%' or el_to_path like '%?utm\\_referrer=%' \
    or el_to_path like '%&utm\\_name=%' or el_to_path like '%?utm\\_name=%' \
    or el_to_path like '%&utm\\_social%' or el_to_path like '%?utm\\_social%' \
    or el_to_path like '%&fbclid=%' or el_to_path like '%?fbclid=%'\
    or (el_to_domain_index like 'https://com.facebook%' \
         and (el_to_path like '%refsrc=%' or el_to_path like '%hrc=%'))\
    or el_to_path like '%&gad_source=%' or el_to_path like '%?gad_source=%' \
    or el_to_path like '%&gclid=%' or el_to_path like '%?gclid=%' \
    or el_to_path like '%&gclsrc=%' or el_to_path like '%?gclsrc=%' \
    or el_to_path like '%&dclid=%' or el_to_path like '%?dclid=%' \
    or el_to_path like '%&gbraid=%' or el_to_path like '%?gbraid=%' \
    or el_to_path like '%&wbraid=%' or el_to_path like '%?wbraid=%' \
    or el_to_path like '%&\\_hs\\_enc=%' or el_to_path like '%?\\_hs\\_enc=%' \
    or el_to_path like '%&\\_hs\\_mi=%' or el_to_path like '%?\\_hs\\_mi=%' \
    or (el_to_domain_index like 'https://com.imdb%' \
        and (el_to_path like '%\\_ref=%'))\
    or el_to_path like '%&igshid=%' or el_to_path like '%?igshid=%' \
    or el_to_path like '%&igsh=%' or el_to_path like '%?igsh=%' \
    or el_to_path like '%&irclickid=%' or el_to_path like '%?irclickid=%' \
    or el_to_path like '%&utm_klaviyo_id=%' or el_to_path like '%?utm_klaviyo_id=%' \
    or el_to_path like '%cv\\_id%' or el_to_path like '%wickedid%' \
    or el_to_path like '%soc\\_src=%' or el_to_path like '%soc_trk=%' \
    or el_to_path like '%\\_openstat=%' or el_to_path like '%yclid=%' \
    or el_to_path like '%\\_hsenc=%' or el_to_path like '%\\_hsmi=%' \
    or el_to_path like '%mc\\_cid=%' or el_to_path like '%mc\\_eid=%' \
    or el_to_path like '%&otc=%' or el_to_path like '%?otc=%'   \
    or el_to_path like '%oly\\_anon\_id=%' or el_to_path like '%oly\\_enc\\_id=%' \
    or el_to_path like '%\\_mongo\_id=%' or el_to_path like '%sb\\_referrer\\_id=%' \
    or (el_to_domain_index like 'https://com.tiktok%' \
         and el_to_path like '%=%') \
    or (el_to_domain_index like 'https://com.youtube%' \
         and ( el_to_path like '%app%' \
             or el_to_path like '%feature%'  or el_to_path like '%kw%')) \
    or el_to_path like '%&wt\\_mc=%' or el_to_path like '%?wt\\_mc=%' \
    or el_to_path like '%&wt\\zmc=%' or el_to_path like '%?wt\\_zmc=%' \
    or el_to_path like '%&cvid=%' or el_to_path like '%?cvid=%' \
    or el_to_path like '%&ocid=%' or el_to_path like '%?ocid=%' \
    or el_to_path like '%&ICID=%' or el_to_path like '%?ICID=%' \
    or el_to_path like '%rb\\_click=%' \
    )  \
    and rc_id > {last_ref}" # or rc_title = 'Q5130305'" # for testing
    pywikibot.output(qrystr)
    datalinks = datacur.execute(qrystr)
    pywikibot.output(datalinks)

    prev_item = ""
    while link_row := datacur.fetchone():
        item = link_row[0]
        pywikibot.output(item)
        if item != prev_item:
            if batchcur.execute(
                "delete from cleanse_refs where item=%s and processed_time is not null",
                item,
            ):
                batchcur.execute(f"DELETE FROM cleanse_refs_url where item=%s", item)
            batchcur.execute(
                f"INSERT IGNORE INTO cleanse_refs (item, scheduled_time) VALUES (%s, NOW())",
                item,
            )
        batchcur.execute(
            f"INSERT IGNORE INTO cleanse_refs_url (item, url) VALUES (%s, %s)", link_row
        )
        batchcur.execute("COMMIT")


    batchcur.execute(
        f"update rc_tracker set last_rc_id = {max_rc_id} where proc_key = 'CLEANSEREFS'"
    )
    batchcur.execute("COMMIT")
