#!/usr/bin/python
"""
Bot to remove dead URLs in references where there is an alternative

The following parameters are supported:

-always           The bot won't ask for confirmation when putting a page

-report_page:     Output list of target references  

-report_only:     Do not update any of the references identified

-nonfree:         Process references where the alternative is non-free

-fix_limit:       Maximum number of pages that will be fixed, for trialling purposes

"""
#
# (C) William Avery 2021
#
# Distributed under the terms of the MIT license.
#
from sys import maxsize
from pathlib import Path
from textwrap import dedent
import re
import pywikibot
from pywikibot import pagegenerators
from pywikibot.page import Page

from pywikibot.bot import (
    SingleSiteBot, ConfigParserBot, CurrentPageBot, NoRedirectPageBot)

import mwparserfromhell

import wikipythonics_util as wpyu

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {'&params;': pagegenerators.parameterHelp}  # noqa: N816

class unneededDeadlinksBot(
    # Refer pywikobot.bot for generic bot classes
    SingleSiteBot,  # A bot only working on one site
    NoRedirectPageBot,
    CurrentPageBot,  # Sets 'current_page'. Process it in treat_page method.
):

    """
    Bot to remove dead URLs in references where there is a free alternative
    """

    def __init__(self, generator, **kwargs) -> None:
        """
        Initializer.

        @param generator: the page generator that determines on which pages
            to work
        @type generator: generator
        """
        # Add your own options to the bot and set their defaults
        # -always option is predefined by BaseBot class
        self.available_options.update({
            'report_page': None,  # name of a page to list cases identified for processing
            'report_only': False,
            'nonfree': None, # Accept non-free alternatives too
            'fix_limit': maxsize
        })

        # call initializer of the super class
        super().__init__(site=True, **kwargs)

        pywikibot.output(f"{pywikibot.calledModuleName()} is instantiating")

        self.brfa = 3                
        self.generator = generator # assign the generator to the bot
        self.options = kwargs
        # Following sources are free if they have an additional parameter: e.g. doi-access=free
        self.maybe_free = ('bibcode', 'doi', 'hdl', 'jstor', 'ol', 'osti', 's2cid') 
        # Following sources are always free
        self.always_free = ('arxiv', 'biorxiv', 'citeseerx', 'rfc', 'ssrn', 'osti', 'pmc') 

        self.fix_limit = int(kwargs['fix_limit'])
        self.page_count = 0
        self.template_not_found = 0   # Count of pages on which no dl template was found
        self.template_count = 0       # Count of dl templates found
        self.no_fix = 0               # Count of dl templates where no fix yet attempted
        self.no_cite_template = 0     # Count of remaining templates that have no associated cite template
        self.no_url=0                 # Count of pages that sould have been fixed, but no url present
        self.no_alternative = 0       # Count of cite templates with no alternative access key defined
        self.nonfree_access = 0       # Count of cite templates with non-free access only
        self.count = 0                # Count of fixable templates, with free access alternative
        self.arch_count = 0           # Count of cite templates found that have an archive-url (!)
        self.sample=[]                # Array of fixable cases for reporting
        self.sample_nonfree=[]        # Array of cases with nonfree access only for reporting
        self.fixed_pages = 0          # Stop when fix_limit is reached
        self.dltemplate_regexp = re.compile(r'^([Pp]ermanent[_ ]dead[_ ]link|[Dd]ead|[Dd]ead[_ ]link|[Dd]ead[_ ]Link|[Dd]eadlink|[Dd]l|[Bb]roken[_ ]link|[Bb]adlink|404|[Bb]adurl|[Ll]ink[_ ]broken|[Dd]eadurl|[Dd]ead[_ ]url)$')
        pywikibot.output(f"{pywikibot.calledModuleName()} instantiated")

    def output_sample(self, sample):
        """
        Formats a list of cites that might be processed, or are of interest
        """
        sample_text = """{|
        ! style="text-align:left;"| Seq
        ! style="text-align:left;"| Dead since
        ! style="text-align:left;"| Tag
        ! style="text-align:left;"| ix
        ! style="text-align:left;"| !Access code
        ! style="text-align:left;"| Page
        |-"""
        for line in sample:
            sample_text+= f"""
        | {line['seq']}
        ! style="text-align:left;"|{line['dead_date']}
        | {line['parent_tag']}
        | {line['index']}
        | {line['access_code']}
        |[[{line['title']}]]
        |-
        |colspan="5"|<nowiki>{line['template']}</nowiki>
        |-
        """

        sample_text+="|}\n"
        return sample_text

    def teardown(self):
        """
        Reporting after all the pages have been processed
        """
        pywikibot.output(dedent(
            f"""
            {pywikibot.calledModuleName()} teardown
            Pages processed: {self.page_count} No template found: {self.template_not_found}
             Dead link templates processed: {self.template_count}
            -No cite template: {self.no_cite_template}
            -No url: {self.no_url}
            -Fixed but tagged dead: {self.arch_count}
            -No alternative: {self.no_alternative}
            -Non free alternative only: {self.nonfree_access}
            -Deadlink fix not attempted: {self.no_fix}
             Fixable: {self.count}
             """))

        if (self.options['report_page']):
            sample_summary = f"{self.count} free samples"
            sample_page_text = '==Free alternatives==\n'
            sample_page_text += self.output_sample(self.sample)
            if self.options['nonfree']:
                sample_summary += f"; {self.nonfree_access} non-free samples"
                sample_page_text += '==Non-free alternatives==\n'
                sample_page_text += self.output_sample(self.sample_nonfree)
            newPage=Page(pywikibot.Site(), self.options['report_page'])
            newPage.text=sample_page_text
            newPage.save(sample_summary)
            

    def treat_page(self) -> None:
        def getx(elem, keys):
            for key in keys:
                if elem.has(key):
                    return elem.get(key)
            return None

        def access(templ, key): # Typical key is 'doi'
            if key=='doi' and templ.has('doi-broken-date') and templ.get('doi-broken-date').value.strip():
                return None
            return key if templ.has(key) and templ.get(key).value.strip() else None
    
        def freeaccess(templ, key): # Typical key is 'pmc'
            freekey=key + '-access'
            if key=='pmc' and templ.has('pmc-embargo-date') and templ.get('pmc-embargo-date').value.strip():
                return None
            return key if access(templ, key) and templ.has(freekey) and templ.get(freekey).value.strip()=='free' else None
        #if not self.current_page.botMayEdit(): return False
        new_text = self.current_page.text
        mwtree = mwparserfromhell.parse(new_text, skip_style_tags=True)

        self.page_count+=1
        templates_found = False
        refs_fixed = 0
        pywikibot.debug(mwtree.get_tree(), 'tree')
        for dl_template in mwtree.filter_templates(matches = lambda t : self.dltemplate_regexp.match(str(t.name).strip())):
            self.template_count += 1
            templates_found = True
            cite_template = None
            fix_attempted = False
            url_param=None
            archive_url = None
            free_access_code=None
            nonfree_access_code=None
            dead_date = None
            if ( dl_template.has('fix-attempted') and dl_template.get('fix-attempted').value.strip() == "yes"):
                fix_attempted = True
            else:
                pywikibot.output(f"{self.current_page.title()} - No fix attempted: {str(dl_template)}")

            if dl_template.has('date'):
                dead_date = dl_template.get('date').value.strip()

            parents = mwtree.get_ancestors(dl_template)
            pywikibot.debug(parents, 'parents')
            # As a fallback, the template's parent is the page itself
            paren = parent_conts = mwtree
            parent_tag ='<page>'
            stop_id = id(dl_template)

            # Find a parent mediawiki tag, if any
            i=-1
            while abs(i) <= len(parents): 
                if type(parents[i]) in (mwparserfromhell.nodes.tag.Tag,
                                        mwparserfromhell.nodes.wikilink.Wikilink):
                    break
                else: 
                    pywikibot.output(f"Unexpected parent: {str(len(parents))} {str(type(parents[i]))}")
                    i-=1

            if abs(i) <= len(parents):
                
                paren = parents[i] # Typically a ref tag
                parent_conts = paren
                parent_tag = str(type(paren))
                if type(paren) == mwparserfromhell.nodes.wikilink.Wikilink:
                    parent_conts = paren.text
                if type(paren) == mwparserfromhell.nodes.tag.Tag:
                    parent_tag = paren.tag 
                    parent_conts = paren.contents
                if i < -1:
                    stop_id = id(parents[i+1]) # parent wasn't a tag, e.g. text node
                    pywikibot.output(f"{self.current_page.title()}: Parent is not a tag - {str(type(parents[i+1]))} : {str(parents[i+1])}")

            
            pywikibot.output(self.current_page.title() + ":" + str(parent_tag) + "/" + str(i))

            # Get the template or external link that immediately precedes deadlink template
            tp2=None
            for tpn in parent_conts.filter(recursive=False,
                        matches=lambda n:
                        type(n) in (mwparserfromhell.nodes.template.Template,
                            mwparserfromhell.nodes.external_link.ExternalLink,
                            mwparserfromhell.nodes.wikilink.Wikilink,
                            mwparserfromhell.nodes.heading.Heading)):
                if id(tpn) == stop_id:
                    break
                else:
                    tp2=tpn

            if tp2 and type(tp2)==mwparserfromhell.nodes.template.Template:
                cite_template = tp2
                archive_url_param=getx(cite_template, ['archive-url', 'archiveurl'])
                archive_url = archive_url_param.value.strip() if archive_url_param else None
                url_param=getx(cite_template, ['url'])
                pywikibot.output(url_param)

                # Check whether the template has free access code
                free_access_code = next(filter(
                    lambda accesscode :
                        freeaccess(cite_template, accesscode), self.maybe_free
                    ), None)

                if not free_access_code:
                    free_access_code = next(filter(
                    lambda accesscode :
                        access(cite_template, accesscode), self.always_free
                    ), None)

                if not free_access_code:
                    nonfree_access_code = next(filter(
                    lambda accesscode :
                        access(cite_template, accesscode), self.maybe_free
                    ), None)

            if fix_attempted and free_access_code and url_param and not archive_url:
                self.count+= 1
                pywikibot.output("#{2} {0} | {4}/{3} access={1}".format(self.current_page.title(),free_access_code, str(self.count),parent_tag, abs(i)))
                self.sample.append(
                    {'title':self.current_page.title(),
                    'seq': self.count,
                    'dead_date': dead_date,
                    'parent_tag': parent_tag,
                    'index': i,
                    'access_code' : free_access_code,
                    'template': str(tp2)
                    }
                )
                if not self.options['report_only']:
                    pywikibot.output(cite_template)
                    cite_template.remove('url')
                    if cite_template.has('access-date'):
                        cite_template.remove('access-date')
                    if cite_template.has('accessdate'):
                        cite_template.remove('accessdate')
                    mwtree.remove(dl_template, recursive = True)
                    refs_fixed+=1

            elif not cite_template:
                self.no_cite_template+=1 
                pywikibot.output("nocite# {0} | {4}/{3} access={1}".format(self.current_page.title(),free_access_code, str(self.count),parent_tag, abs(i)))               

            elif not url_param:
                self.no_url+=1
                pywikibot.output("nourl# {0} | {4}/{3} access={1}".format(self.current_page.title(),free_access_code, str(self.count),parent_tag, abs(i)))               
            elif not (nonfree_access_code or free_access_code):
                self.no_alternative+=1
                pywikibot.output("noalt# {0} | {4}/{3} access={1}".format(self.current_page.title(),free_access_code, str(self.count),parent_tag, abs(i)))
            elif (nonfree_access_code):
                self.nonfree_access+=1
                self.sample_nonfree.append(
                    {'title':self.current_page.title(),
                    'seq': self.nonfree_access,
                    'dead_date': dead_date,
                    'parent_tag': parent_tag,
                    'index': i,
                    'access_code' : nonfree_access_code,
                    'template': str(tp2)
                    }
                )
            elif archive_url:
                self.arch_count+= 1
                pywikibot.output("#a{2} {0} | {4}/{3} access={1}".format(self.current_page.title(),free_access_code, str(self.count), parent_tag, abs(i)))
            elif not fix_attempted:                
                self.no_fix+=1
            else: 
                pywikibot.output(self.current_page.title() + "!!" + str(parent_tag))
                pywikibot.output(str(type(tp2)) + " " + str(tp2)) if tp2 else None

        # Some dl templates are within included templates, but  might be an error
        if not templates_found:
            self.template_not_found+=1
            pywikibot.output(f"{self.current_page.title()} !!No template found")
        
        if refs_fixed:
            summary = f"Dead URLs removed where ref contains free alternative (×{refs_fixed}). See [[WP:Bots/Requests for approval/William Avery Bot {self.brfa}|BRFA]]" + wpyu.enwiki_apply_fixes(mwtree)
            self.put_current(str(mwtree), summary=summary)
            self.fixed_pages+=1
            if self.fixed_pages == self.fix_limit:
                pywikibot.output(f"Reached limit of {self.fix_limit} pages to be fixed")
                self.quit()



def main(*args: tuple([str, ...])) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    """
    options = {'report_page': None, 'report_only':False, 'nonfree':None, 'fix_limit': maxsize}
    # Process global arguments to determine desired site
    local_args = pywikibot.handle_args(args)

    # This factory is responsible for processing command line arguments
    # that are also used by other scripts and that determine which pages
    # to work on.
    gen_factory = pagegenerators.GeneratorFactory()

    # Process pagegenerators arguments
    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(':')
        option = arg[1:]
        if option in ('report_page', 'fix_limit'):
            if not value:
                pywikibot.input('Please enter a value for ' + arg)
            options[option] = value
        # take the remaining options as booleans.
        else:
            options[option] = True

    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        bot = unneededDeadlinksBot(gen, **options)
        bot.run() 
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == '__main__':
    main()
