#
# (C) William Avery, 2022
#
# Distributed under the terms of the MIT license.
#
import pywikibot
from pywikibot import pagegenerators
site=pywikibot.Site("wikipedia:en")

pg=pywikibot.Page(site, 'User:SDZeroBot/G13_soon_sorting')

draftns=site.namespace(118)

cat = pywikibot.Category(pywikibot.Page(site, 'Category:AfC G13 eligible soon submissions'))
purgelist=[]
for pge in pg.linkedPages():
    print(pge.title())
    try: 
        if pge.namespace() != draftns:
            continue
        cats=list(pge.categories())
        if cat in cats:
            continue
        purgelist.append(pge)
        if len(purgelist) > 3:
            site.purgepages(purgelist, forcelinkupdate=True)
            purgelist=[]
    except Exception as e:
        print(e)
    if purgelist:
        site.purgepages(purgelist, forcelinkupdate=True)
