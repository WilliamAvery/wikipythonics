python3 $PYWIKIBOT_DIR/pwb.py unneededDeadlinksBot.py \
-mysqlquery:"\
select p.page_namespace, p.page_title \
from page p \
join page tp on tp.page_title=p.page_title and p.page_namespace = 0 and tp.page_namespace = 1 \
where exists (\
   select 1  from categorylinks cl  \
             join category c on cl_to=c.cat_title \
             where cl_from=tp.page_id and c.cat_title='All_WikiProject_Medicine_articles') \
and exists (\
   select 1  from categorylinks cl2 \
             join category c2 on cl2.cl_to=c2.cat_title \
             where cl2.cl_from=p.page_id and c2.cat_title='Articles_with_permanently_dead_external_links') \
order by p.page_title" \
 -report_only -report_page:User:William_Avery_Bot/testsample_medicine
