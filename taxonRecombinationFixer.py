#!/usr/bin/python
"""
Use global -simulate option for test purposes. No changes to live wiki
will be done.

The following generators and filters are supported but
cannot be set by settings file:

&params;
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2021
# (C) William Avery, 2021
#
# Distributed under the terms of the MIT license.
#

import json

import pywikibot
from pywikibot import pagegenerators
from pywikibot.page import WikibasePage
from pywikibot.bot import (
    ExistingPageBot,
    SingleSiteBot
)

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {'&params;': pagegenerators.parameterHelp}


class taxonRecombinationFixer(
    ExistingPageBot,
    SingleSiteBot
):

    def __init__(self, generator, **kwargs):
        self.available_options.update({
        })

        super(taxonRecombinationFixer, self).__init__(**kwargs)

        self.generator = generator

        if self.site == self.site.getSite('wikidata'):
            self.id_taxon_name='P225'
            self.id_instance_of='P31'
            self.id_has_role = 'P3831'
            self.id_recombination='Q14594740'

        elif self.site == self.site.getSite('test'):
            # Stand-ins used for proof of concept
            self.id_taxon_name='P49'
            self.id_instance_of='P82'
            self.id_has_role = 'P95819'   # has edition or translation
            self.id_recombination='Q1979' # test item
        else:
            raise Exception(f"Cannot determine entity ids for {self.site}")

        self.json_for_role_prop=json.loads(f'''
                   {{
                    "snaktype": "value",
                    "property": "{self.id_has_role}",
                    "datatype": "wikibase-item",
                    "datavalue": 
                        {{
                        "value": 
                            {{
                            "entity-type": "item", 
                            "numeric-id": {int(self.id_recombination[1:])}
                            }}, 
                        "type": "wikibase-entityid"
                        }}
                    }}''')

    def teardown(self):
        pass

    def treat_page(self) -> None:
        
        p_item = self.current_page

        if not self.id_taxon_name in p_item.claims:
            pywikibot.warn(f"No taxon name property ({self.id_taxon_name})")
            return

        taxonnameprops = p_item.claims.get(self.id_taxon_name)

        if not taxonnameprops or len(taxonnameprops) < 1:
            pywikibot.warn(f"Only expecting one occurrence of taxon name ({self.id_taxon_name})")
            return

        taxonnameprop=taxonnameprops[0]
        
        if self.id_instance_of not in taxonnameprop.qualifiers:
            pywikibot.warn(f"No 'instance of' ({self.id_instance_of}) qualifier found on taxon name")
            return

        instance_of_quals = taxonnameprop.qualifiers.get(self.id_instance_of)

        if not instance_of_quals or len(instance_of_quals) < 1:
            pywikibot.warn(f"Expecting to find one 'instance of' ({self.id_instance_of}) qualifier")
            return

        instance_of_qual = instance_of_quals[0]

        if not isinstance(instance_of_qual.target, WikibasePage) or not instance_of_qual.target_equals(self.id_recombination):
            pywikibot.warn(f"Expecting occurrence of recombination ({self.id_recombination}') but got {instance_of_qual.target}")
            return

        del(taxonnameprop.qualifiers[self.id_instance_of])

        role_qualifier = taxonnameprop.qualifierFromJSON(self.site, self.json_for_role_prop)

        taxonnameprop.qualifiers[self.id_has_role]=[role_qualifier]

        p_item.editEntity(summary="Change recombination qualifier from 'instance of' to 'object has role'. See [[Wikidata:Requests_for_permissions/Bot/William_Avery_Bot_2|request for permission]]")
        
def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {}

    local_args = pywikibot.handle_args(args)

    gen_factory = pagegenerators.GeneratorFactory()

    local_args = gen_factory.handle_args(local_args)

    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        bot = taxonRecombinationFixer(generator=gen, **options)
        bot.run() 
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == '__main__':
    main()
