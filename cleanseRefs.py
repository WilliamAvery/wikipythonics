#!/usr/bin/python3
"""
One-off Wikidata script to change qualifiers on values of family name (P734)
from Criterion Used (P1013) to Object Has Role (P3831).
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2022
# Tracking parameters from https://github.com/jparise/chrome-utm-stripper
# (C) William Avery, 2022
#
# Distributed under the terms of the MIT license.
#
import re
import urllib
import pywikibot
from pywikibot.bot import (
    ExistingPageBot,
    SingleSiteBot,
    WikidataBot
)
import pagegenerators_ext as pagegenerators

class CleanseRefs(
    WikidataBot
):
    def __init__(self, **kwargs):
        super(CleanseRefs, self).__init__(**kwargs)

        self.options = kwargs  

        # List of tracker parameters 
        # Based on https://github.com/jparise/chrome-utm-stripper#recognized-parameters  
        self.trackers = {
            # Adobe
            'ef_id': None,
            's_kwcid': None,
            # Facebook
            'fbclid' : None,
            'refsrc' : re.compile('(.+\.)?facebook.com'),
            'hrc' : re.compile('(.+\.)?facebook.com'),
            # Google
            'gad_source' : None,
            'gclid' : None,
            'gclsrc': None,
            'utm_source' : None,
            'utm_medium' : None,
            'utm_term' : None,
            'utm_campaign': None,
            'utm_content': None,
            'utm_cid': None,
            'utm_reader': None,
            'utm_referrer': None,
            'utm_name': None,
            'utm_social': None,
            'utm_social-type': None,
            'dclid': None,
            'gbraid': None,
            'wbraid': None,
            # HubSpot
            '_hsenc': None,
            '_hsmi': None,
            # imdb
            'ref_': re.compile('(.+\.)?imdb.com'),
            # Instagram
            'igshid': None,
            'igsh' : re.compile('(.+\.)?instagram.com'),
            # impact.com
            'irclickid': None,
            # Klaviyo https://www.klaviyo.com
            'utm_klaviyo_id': None,
            #Mailchimp
            'mc_cid': None,
            'mc_eid': None,
            # Marketo
            'mkt_tok': None,
            # Microsoft (MSN/Bing)
            'cvid': None,
            'ocid': None,  # typo in source
            'msclkid': None,
            # Olytics
            'oly_anon_id': None,
            'oly_enc_id': None,
            'otc': None,
            # Springbot
            'redirect_log_mongo_id': None,
            'redirect_mongo_id': None,
            'sb_referer_host': None,
            #TikTok
            '_d': re.compile('(.+\.)?tiktok.com'),
            'checksum': re.compile('(.+\.)?tiktok.com'),
            'is_copy_url': re.compile('(.+\.)?tiktok.com'),
            'is_from_webapp': re.compile('(.+\.)?tiktok.com'),
            'lang': re.compile('(.+\.)?tiktok.com'),
            'language': re.compile('(.+\.)?tiktok.com'),
            'preview_pb': re.compile('(.+\.)?tiktok.com'),
            'sec_uid': re.compile('(.+\.)?tiktok.com'),
            'sec_user_id': re.compile('(.+\.)?tiktok.com'),
            'sender_device': re.compile('(.+\.)?tiktok.com'),
            'sender_web_id': re.compile('(.+\.)?tiktok.com'),
            'share_app_id': re.compile('(.+\.)?tiktok.com'),
            'share_author_id': re.compile('(.+\.)?tiktok.com'),
            'share_link_id': re.compile('(.+\.)?tiktok.com'),
            'share_item_id': re.compile('(.+\.)?tiktok.com'),
            'source': re.compile('(.+\.)?tiktok.com'),
            'timestamp': re.compile('(.+\.)?tiktok.com'),
            'tt_from': re.compile('(.+\.)?tiktok.com'),
            'u_code': re.compile('(.+\.)?tiktok.com'),
            'user_id': re.compile('(.+\.)?tiktok.com'),
            # Wicked Reports
            'wickedid': None,
            # Yahoo
            'soc_src': None,
            'soc_trk': None,
            # Yandex
            '_openstat': None,
            'yclid': None,
            # YouTube
            'app' : re.compile('(.+\.)?youtube.com'),
            'feature' : re.compile('(.+\.)?youtube.com'),
            'kw' : re.compile('(.+\.)?youtube.com'),
            # zeit.de
            'wt_mc': None, 
            'wt_zmc': None,
            # Other
            'ICID': None,
            'rb_clickid': None
            # stm_ variants of the utm_ parameters
        }

    # Decorator will call db proc to mark item as processed, if option set
    @pagegenerators.batchcontrol
    def treat_page(self) -> None:
        replaced_count = 0
        for prop in self.current_page.claims:
            claim_list = self.current_page.claims.get(prop)
            for claim in claim_list:
                for source in claim.sources:
                    if not 'P854' in source:
                        continue

                    for ref_url_claim in source['P854']:

                        ref_url=ref_url_claim.getTarget()
                        if not ref_url:
                            continue 

                        parsed_url = urllib.parse.urlparse(ref_url)
                        if not parsed_url.query:
                            continue
                        parsed_q = urllib.parse.parse_qs(parsed_url.query, keep_blank_values=True, strict_parsing=False)
                        pywikibot.output(f"Processing ({prop} - {parsed_q})")
                        del_count = 0
                        for param in list(parsed_q.keys()):
                            if (param in list(self.trackers.keys()) 
                                and (self.trackers[param] is None
                                 or self.trackers[param].match(parsed_url.hostname))) :
                                del parsed_q[param]
                                pywikibot.output(f"Deleted {param}")
                                del_count+=1
                        if del_count:
                            parsed_url= parsed_url._replace(query=urllib.parse.urlencode(parsed_q, doseq=True))
                            ref_url_claim.setTarget(urllib.parse.urlunparse(parsed_url))
                            pywikibot.output(ref_url_claim.getTarget())
                            replaced_count+=1
        if not replaced_count: return True
        
        summary = f"Cleansing reference URLs (×{replaced_count})"
        # 'Always update' option is assumed to indicate bot operation
        if "always" in self.options and self.options["always"]:
            summary += ". See [[Wikidata:Requests_for_permissions/Bot/William_Avery_Bot_9|request for permission]]"
            self.current_page.editEntity(summary=summary)
        else:
            self.user_edit_entity(self.current_page, summary=summary)

        return True

def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {}
    # Process global arguments to determine desired site
    local_args = pywikibot.handle_args(args)

    # This factory is responsible for processing command line arguments
    # that are also used by other scripts and that determine on which pages
    # to work on.
    gen_factory = pagegenerators.GeneratorFactory()

    # Process pagegenerators arguments
    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(":")
        if arg[0] != '-':
            pywikibot.output(f"Error on '{arg}' - Command line options start with hyphen")
            raise SystemExit(1)
        option = arg[1:]
        if option == 'always':
            if value:
                pywikibot.output(f"Command line option '{arg}' takes no value")
                raise SystemExit(1)
            options[option] = True
        else:
            pywikibot.output(f"Did not understand command line option '{arg}'")
            raise SystemExit(1)

    # The preloading option is responsible for downloading multiple
    # pages from the wiki simultaneously.
    gen = gen_factory.getCombinedGenerator(preload=True)

    # check if further help is needed
    if not pywikibot.bot.suggest_help(missing_generator=not gen):
        # pass generator and private options to the bot
        bot = CleanseRefs(generator=gen, **options)
        bot.run()
''

if __name__ == '__main__':
    main()
