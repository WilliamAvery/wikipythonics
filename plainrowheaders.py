#!/usr/bin/python
"""
Bot to add '{{Plain row headers}}' before tables where required, or remove
plainrowheaders class from tables where it's superfluous.

The following parameters are supported:

-always           The bot won't ask for confirmation when putting a page

-fix_limit:       Maximum number of pages that will be fixed, for trialling purposes

"""
#
# (C) William Avery 2022
#
# Distributed under the terms of the MIT license.
#
import re
from sys import maxsize
from pathlib import Path
import pywikibot
from pywikibot.page import Page
from pywikibot.bot import SingleSiteBot, CurrentPageBot

import mwparserfromhell

import pagegenerators_ext as pagegenerators


class plainrowheaderBot(
    # Refer pywikobot.bot for generic bot classes
    SingleSiteBot,  # A bot only working on one site
    CurrentPageBot,  # Sets 'current_page'. Process it in treat_page method.
):

    """
    Bot to help automate changes to plainrow table styling
    """

    def __init__(self, generator, **kwargs) -> None:
        """
        Initializer.

        @param generator: the page generator that determines on which pages
            to work
        @type generator: generator
        """

        # -always option is predefined by BaseBot class
        self.available_options.update({"fix_limit": maxsize})

        # call initializer of the super class
        super().__init__(site=True, **kwargs)

        pywikibot.output(f"{pywikibot.calledModuleName()} is instantiating")

        self.brfa = 5
        self.generator = generator  # assign the generator to the bot
        self.options = kwargs
        self.fix_limit = int(kwargs["fix_limit"])
        self.page_count = 0
        self.fixed_pages = 0

        self.template_name = "plain row headers"
        self.old_class_name = "plainrowheaders"
        self.new_class_name = "plain-row-headers"
        self.discovery_count = {
            "total": 0,
            "parser": 0,
            "re": 0,
            "total_expanded": 0,
            "on_expansion": 0,
        }
        pywikibot.output(f"{pywikibot.calledModuleName()} instantiated")

    def teardown(self):
        """
        Reporting after all the pages have been processed
        """
        pywikibot.output(self.discovery_count)
        pywikibot.output(f"Fixed {self.fixed_pages} pages")

    def check_table_attributes(self, table_tag):
        rowscope_present = False
        for row in table_tag.ifilter_tags(
            matches=lambda table_elem: table_elem.tag == "th"
            and table_elem.has("scope")
            and table_elem.get("scope").value in ("row", "rowgroup")
        ):
            rowscope_present = True
            break
        return rowscope_present

    def treat_page(self) -> None:

        mwtree = mwparserfromhell.parse(self.current_page.text, skip_style_tags=True)

        self.page_count += 1
        table_count = 0
        template_style = None

        # pywikibot.debug(mwtree.get_tree(), layer="tree")
        # Insert nodes while filtering the tree can cause loop, so evaluate to list
        for mwnode in list(
            mwtree.ifilter(
                forcetype=(
                    mwparserfromhell.nodes.template.Template,
                    mwparserfromhell.nodes.tag.Tag,
                )
            )
        ):
            if type(
                mwnode
            ) == mwparserfromhell.nodes.template.Template and mwnode.name.matches(
                self.template_name
            ):
                template_style = mwnode
            if type(mwnode) == mwparserfromhell.nodes.tag.Tag and mwnode.tag == "table":
                table_count += 1
                self.discovery_count["total"] += 1
                table_tag = mwnode
                table_classes = ""
                rowscope_present = False
                pywikibot.output(f"Processing table {table_count}...")
                if table_tag.has("class"):
                    table_classes_attr = table_tag.get("class")
                    table_classes = table_classes_attr.value
                class_list = table_classes.split()

                for i, class_name in enumerate(class_list):
                    if class_name == self.old_class_name:
                        class_list[i] = self.new_class_name

                if self.new_class_name in class_list:

                    rowscope_present = self.check_table_attributes(table_tag.contents)
                    if rowscope_present:
                        self.discovery_count["parser"] += 1
                    # Here be templates?
                    if not rowscope_present:
                        for row in table_tag.contents.ifilter_tags(
                            matches=lambda table_elem: table_elem.tag == "th"
                        ):
                            if mtch := re.search(
                                "(scope\s*=\s*[\"']?row[\"']?)\s*((\{\{[^}^|]+(}}|\|))?)",
                                str(row),
                            ):
                                rowscope_present = True
                                self.discovery_count["re"] += 1
                                # This is always more or less pathological
                                pywikibot.warning(
                                    f"<{mtch.group(1)}> + <{mtch.group(2)}> interpreted as scope=row at [[{self.current_page.title()}]] table {table_count}"
                                )
                                break

                        if mtch := re.search(
                            "\{\{\s*([Ee]pisode[ _]list|[Ss]ingle[ _]chart)\s*\|",
                            str(table_tag),
                        ):
                            print(f"Table row template <{mtch.group(1)}>")
                            self.discovery_count["re"] += 1
                            rowscope_present = True

                    if not rowscope_present and str(table_tag).find("{{") > -1:
                        pywikibot.output(
                            f"Expanding text at [[{self.current_page.title()}]] table {table_count}"
                        )
                        self.discovery_count["total_expanded"] += 1
                        expanded_text = self.current_page.site.expand_text(
                            str(table_tag),
                            title=self.current_page.title(with_section=False),
                            includecomments=False,
                        )
                        table_tree = mwparserfromhell.parse(
                            expanded_text, skip_style_tags=True
                        )
                        if rowscope_present := self.check_table_attributes(table_tree):
                            self.discovery_count["on_expansion"] += 1
                            pywikibot.warning(
                                f"Expansion of templates was required at [[{self.current_page.title()}]] table {table_count}"
                            )

                    pywikibot.output(f"{template_style=} {rowscope_present=}")
                    if not rowscope_present or "wikitable" not in class_list:
                        pywikibot.output("Removing class")
                        class_list.remove(self.new_class_name)
                        table_classes_attr.value = " ".join(class_list)
                    elif not template_style:
                        pywikibot.output("Adding template")
                        mwtree.insert_before(
                            table_tag, f"{{{{{self.template_name}}}}}\n"
                        )

                    table_classes_attr.value = " ".join(class_list)

                template_style = None

        newtext = str(mwtree)
        if newtext != self.current_page.text:
            prots = self.current_page.protection()
            if "edit" in prots and "sysop" in prots["edit"]:
                pywikibot.warning(f"cannot edit {self.current_page.title()}")
                return 1

            self.fixed_pages += 1

            self.put_current(
                newtext,
                summary=f"Applying [[Wikipedia:TemplateStyles|TemplateStyles]] for plainrowheaders. See [[WP:Bots/Requests for approval/William Avery Bot {self.brfa}|BRFA]]",
            )

            if self.fixed_pages == self.fix_limit:
                pywikibot.output(f"Reached limit of {self.fix_limit} pages to be fixed")
                self.quit()


def main(*args: tuple([str, ...])) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    """
    options = {"fix_limit": maxsize}
    # Process global arguments to determine desired site
    local_args = pywikibot.handle_args(args)

    # This factory is responsible for processing command line arguments
    # that are also used by other scripts and that determine which pages
    # to work on.
    gen_factory = pagegenerators.GeneratorFactory()

    # Process pagegenerators arguments
    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(":")
        option = arg[1:]
        if option in ("fix_limit"):
            if not value:
                pywikibot.input("Please enter a value for " + arg)
            options[option] = value
        # take the remaining options as booleans.
        else:
            options[option] = True

    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        bot = plainrowheaderBot(gen, **options)
        bot.run()


if __name__ == "__main__":
    main()
