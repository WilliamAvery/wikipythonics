#!/usr/bin/python
"""
One-off Wikidata script to replace reference URL (P854) with  Holocaust.cz person ID (P9109) in references 
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2022
# (C) William Avery, 2022
#
# Distributed under the terms of the MIT license.
#

import re

import pywikibot

from pywikibot import pagegenerators, PropertyPage
import pagegenerators_ext

from pywikibot.bot import (
    WikidataBot
)

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {'&params;': pagegenerators.parameterHelp}


class UrlRefToId(
    WikidataBot,  # Only used to get user_edit_entity method for testing, treat_page is overridden
):

    def __init__(self, **kwargs):
        self.available_options.update({
        })

        super(UrlRefToId, self).__init__(**kwargs)

        self.options = kwargs

        self.stated_in = 'P248'
        self.ref_id = 'P9109' # 'Holocaust.cz person ID
        self.ref_url = 'P854' 

        self.stated_in_val = 'Q104074149' # The Database of Victims of the Nazi Persecution

    def teardown(self):
        pass

    def find_best_claim(self, claims):
            """Find the first best ranked claim.""" 
            if not claims:
                return None       
            #from pywikibot.page
            index = None
            for i, claim in enumerate(claims):
                if claim.rank == 'preferred':
                    return claim
                if index is None and claim.rank == 'normal':
                    index = i
            if index is None:
                index = 0
            return claims[index]

    def new_claim(self, prop, val, **kwargs) -> pywikibot.page.Claim:
        """Returns a new claim set to the given value"""
        clm = PropertyPage(self.site, prop).newClaim(**kwargs)
        clm.setTarget(val)
        return clm

    def treat_page(self) -> None:
        
        p_item = self.current_page

        add_count = 0
        changed_count = 0
        idclaim = None

        for prop in self.current_page.claims:
            claim_list = self.current_page.claims.get(prop)
            for claim in claim_list:
                for source in claim.sources:
                    changed = False
                    # Only process if source has "stated in The Database of Victims of the Nazi Persecution" 
                    if not self.stated_in in source:
                        continue
                    if not source[self.stated_in][0].target_equals(self.stated_in_val):
                        continue
                    if not self.ref_url in source:
                        continue
                    pywikibot.output(f"Processing ({prop})")

                    # The person should have the ID as a claim on the entity
                    if not idclaim:
                        if self.ref_id not in self.current_page.claims:
                            pywikibot.warn(f"No claim found for {self.ref_id}")
                            continue
                        idclaims= self.current_page.claims[self.ref_id] 
                        idclaim=self.find_best_claim(idclaims)

                    # The ID should match numeric part of the ref URL
                    urlclaim = str(source[self.ref_url][0].getTarget())
                    idfromurl = re.sub(r'[^0-9]', '', urlclaim)
                    if not idclaim.target_equals(idfromurl):
                            pywikibot.warn(f"{self.ref_id} value '{idclaim.getTarget()}' <> '{idfromurl}'")
                            continue

                    # Only add the id to the source if there's not already one there 
                    if self.ref_id not in source:
                        source[self.ref_id] = [self.new_claim(self.ref_id, idfromurl, is_reference=True)]
                        changed = True

                    # only remove the ref URL from the source if it corresponds to the id that is now present
                    if source[self.ref_id][0].target_equals(idfromurl):
                        del source[self.ref_url]
                        changed=True
                    else:
                        pywikibot.warn(f"'Ref ID { source[self.ref_id][0].getTarget()}' did not correspond to '{urlclaim}'")

                    if changed:
                        changed_count += 1

        if not changed_count: return True 

        summary = f"Replacing reference URL (P854) with Holocaust.cz person ID (P9109) (×{changed_count})"

        # 'Always update' option is assumed to indicate bot operation
        if "always" in self.options and self.options["always"]:
            summary += ". See [[Wikidata:Requests_for_permissions/Bot/William_Avery_Bot_5|request for permission]]"
            self.current_page.editEntity(summary=summary)
        else:
            self.user_edit_entity(self.current_page, summary=summary)
        
def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {}

    local_args = pywikibot.handle_args(args)

    gen_factory = pagegenerators_ext.GeneratorFactory()

    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(":")
        option = arg[1:]
        options[option] = True

    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        options["generator"] = gen
        bot = UrlRefToId(**options)
        bot.run() 
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == '__main__':
    main()
