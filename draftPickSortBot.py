#!/usr/bin/python
"""
Bot to sort tables of MLB draft picks into ascending chronological order

The following parameters are supported:

-always           The bot won't ask for confirmation when putting a page

-fix_limit:       Maximum number of pages that will be fixed, for trialling purposes

-remove_template  Remove the 'Chronological' template when a table is sorted

-report_only:     Do not update any of the mainspace pages

-report_page:     Output full text of sorted tables to this page for verification  
"""
#
# (C) William Avery 2021
#
# Distributed under the terms of the MIT license.
#
from sys import maxsize
from pathlib import Path
import re
import pywikibot
from pywikibot import pagegenerators
from pywikibot.page import Page

from pywikibot.bot import (
    SingleSiteBot, CurrentPageBot, NoRedirectPageBot)

import mwparserfromhell

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {'&params;': pagegenerators.parameterHelp}

class draftPickSortBot(
    # Refer pywikobot.bot for generic bot classes
    SingleSiteBot,  # A bot only working on one site
    NoRedirectPageBot,
    CurrentPageBot,  # Sets 'current_page'. Process it in treat_page method.
):

    """
    Bot to sort tables of MLB draft picks into ascending chronological order
    """

    def __init__(self, generator, **kwargs) -> None:
        """
        Initializer.

        @param generator: the page generator that determines on which pages
            to work
        @type generator: generator
        """

        # -always option is predefined by BaseBot class
        self.available_options.update({
            'report_page': None,  # name of a page to list cases identified for processing
            'report_only': False,
            'remove_template': False,
            'fix_limit': maxsize
        })

        # call initializer of the super class
        super().__init__(site=True, **kwargs)

        pywikibot.output(f"{pywikibot.calledModuleName()} is instantiating")

        self.brfa = 4
        self.generator = generator  # assign the generator to the bot
        self.options = kwargs
        self.fix_limit = int(kwargs['fix_limit'])
        self.page_count = 0
        self.fixed_pages = 0
        self.report_text = ''
        self.italic_trimmer = re.compile("^''|''$")

        pywikibot.output(f"{pywikibot.calledModuleName()} instantiated")

    def teardown(self):
        """
        Reporting after all the pages have been processed
        """
        pywikibot.output(f"Fixed {self.fixed_pages} pages")
        if (self.options['report_page']):
            report_page = Page(pywikibot.Site(), self.options['report_page'])
            report_page.text = self.report_text if self.report_text else 'Nothing to report'
            report_page.save("Examples of sorted tables")

    def treat_page(self) -> None:
        def sortf(r):
            return r['year'] + str(r['rowno']).rjust(4, '0')

        mwtree = mwparserfromhell.parse(
            self.current_page.text, skip_style_tags=True)

        self.page_count += 1

        pywikibot.debug(mwtree.get_tree(), 'tree')
        for section in mwtree.get_sections():
            template_node = None
            for mwnode in section.ifilter():
                if type(mwnode) == mwparserfromhell.nodes.template.Template and mwnode.name == 'Chronological':
                    template_node = mwnode
                if type(mwnode) == mwparserfromhell.nodes.tag.Tag and mwnode.tag.matches('table') and template_node:
                    tag = mwnode
                    tag_save = str(tag)
                    pywikibot.output('found table')
                    rows = tag.contents.filter_tags(
                        matches=lambda t: t.tag.matches('tr'))
                    header = rows[0]
                    hcells = header.contents.filter_tags(
                        matches=lambda t: t.tag.matches('th'))
                    # The table must have a header, with the first column labelled as Year
                    if len(hcells) and hcells[0].contents.strip() == 'Year':
                        drows = []
                        rowno = 0
                        rowspan = 0
                        year = 0
                        yearstring = None
                        errormsg = None
                        pywikibot.output(str(rows[-1]).strip())
                        # Most of the tables seem to have a final blank row, which we will leave untouched
                        lastrow = -1 if str(rows[-1]).strip() == '|-' else None
                        for row in rows[1:lastrow]:
                            firstdatacell = None
                            thcells = row.contents.filter_tags(
                                matches=lambda t: t.tag.matches('th'))
                            if len(thcells) > 1:
                                errormsg = "Too many th cells:\n" + str(row)
                                break  # Skip table: we don't deal with more than one header row
                            datacells = row.contents.filter_tags(
                                matches=lambda t: t.tag.matches(('td', 'th')))  # year sometimes bolded, i.e. th.
                            if len(datacells):
                                firstdatacell = datacells[0]
                                if not rowspan:
                                    # The years are sometimes linked
                                    yearstring = firstdatacell.contents.strip_code().strip()
                                    if len(yearstring) > 4:
                                        pywikibot.output(
                                            "!!yearstring=" + yearstring)
                                    yearstring = self.italic_trimmer.sub(
                                        "", yearstring).ljust(5)
                                    if yearstring[4].isdigit() or not yearstring[:3].isdigit():
                                        errormsg = "Expected year but found: " + \
                                            yearstring + " \n" + str(row)
                                        break
                                    year = int(yearstring[:4])
                                    if not 1936 <= year <= 2021:
                                        errormsg = "Year value was not in range 1936-2021: " + \
                                            yearstring + " \n" + str(row)
                                        break
                                if firstdatacell.has('rowspan'):
                                    rowspanstring = firstdatacell.get(
                                        'rowspan').value.strip()
                                    # bad markup accepted by mediawiki
                                    if rowspanstring[0] == '"':
                                        pywikibot.output(
                                            "Questionable rowspan: " + rowspanstring + "\n" + str(row))
                                        rowspanstring = rowspanstring[1:]
                                    # bad markup accepted by mediawiki
                                    if rowspanstring[-1] == '"':
                                        pywikibot.output(
                                            "Questionable rowspan: " + rowspanstring + "\n" + str(row))
                                        rowspanstring = rowspanstring[:-1]
                                    if not rowspanstring.isdigit():
                                        errormsg = "Expected rowspan but found: " + \
                                            rowspanstring + "\n" + str(row)
                                        break
                                    rowspan = int(rowspanstring)
                                for dc in datacells[1:]:
                                    if dc.has('rowspan'):
                                        errormsg = "Rowspan not on year cell: " + \
                                            "\n" + str(row)
                                if errormsg:
                                    break
                                drows.append({'rowspan': rowspan,
                                              'year': yearstring,
                                              'rowno': rowno,
                                              'row': row
                                              })
                                if rowspan:
                                    rowspan -= 1
                            else:
                                pywikibot.output(
                                    "Dropping a row with no data cells: " + str(rowno))
                            tag.contents.remove(row)
                            rowno += 1

                        if errormsg:
                            pywikibot.output(f"At row {rowno} - {errormsg}")
                            pywikibot.output(
                                "Table processing could not be completed")
                            continue
                        drows.sort(key=sortf, reverse=True)
                        for drow in drows:
                            tag.contents.insert_after(rows[0], drow['row'])
                        table_text = str(tag)
                        if table_text != tag_save:
                            pywikibot.output('table sorted')
                            if self.options['remove_template']:
                                mwtree.remove(template_node)
                            if self.options['report_page']:
                                self.report_text += f"==[[{self.current_page.title()}]]==\n{table_text}\n\n"
                        else:
                            pywikibot.output('table already sorted?')
                    else:
                        pywikibot.output(
                            "Table did not have year as first column")

                    template_node = None

        newtext = str(mwtree)
        if newtext != self.current_page.text:
            self.fixed_pages += 1
            if not self.options['report_only']:
                self.put_current(
                    newtext, summary=f"Table of draft selections sorted into ascending chronological order. See [[WP:Bots/Requests for approval/William Avery Bot {self.brfa}|BRFA]]")

            if self.fixed_pages == self.fix_limit:
                pywikibot.output(
                    f"Reached limit of {self.fix_limit} pages to be fixed")
                self.quit()


def main(*args: tuple([str, ...])) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    """
    options = {'report_page': None, 'report_only': False, 'remove_template': False, 'fix_limit': maxsize}
    # Process global arguments to determine desired site
    local_args = pywikibot.handle_args(args)

    # This factory is responsible for processing command line arguments
    # that are also used by other scripts and that determine which pages
    # to work on.
    gen_factory = pagegenerators.GeneratorFactory()

    # Process pagegenerators arguments
    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(':')
        option = arg[1:]
        if option in ('report_page', 'fix_limit'):
            if not value:
                pywikibot.input('Please enter a value for ' + arg)
            options[option] = value
        # take the remaining options as booleans.
        else:
            options[option] = True

    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        bot = draftPickSortBot(gen, **options)
        bot.run()
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == '__main__':
    main()
