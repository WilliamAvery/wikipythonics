#!/usr/bin/python
"""
One-off Wikidata script to fix Minor Planet Center references.
references with
    imported from Wikimedia project (P143) Minor Planet Center (Q522039) 
 are changed to:
    stated in (P248) Minor Planet Center Database (Q106600690)
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2022
# (C) William Avery, 2022
#
# Distributed under the terms of the MIT license.
#
import pywikibot

from pywikibot import pagegenerators, PropertyPage
import pagegenerators_ext

from pywikibot.bot import (
    WikidataBot
)

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {'&params;': pagegenerators.parameterHelp}


class FixMpcRefs(
    WikidataBot,  # Only used to get user_edit_entity method for testing, treat_page is overridden
):

    def __init__(self, **kwargs):
        self.available_options.update({
        })

        super(FixMpcRefs, self).__init__(**kwargs)

        self.options = kwargs

        self.imported_from = 'P143'
        self.stated_in = 'P248'

        self.imported_from_old_val = pywikibot.ItemPage(self.site, 'Q522039') # Minor Planet Center
        self.stated_in_new_val = pywikibot.ItemPage(self.site, 'Q106600690') # Minor Planet Center Database

    def teardown(self):
        pass

    def new_claim(self, prop, val, **kwargs) -> pywikibot.page.Claim:
        """Returns a new claim set to the given value"""
        clm = PropertyPage(self.site, prop).newClaim(**kwargs)
        clm.setTarget(val)
        return clm

    def treat_page(self) -> None:
        
        p_item = self.current_page

        add_count = 0
        changed_count = 0
        idclaim = None

        for prop in self.current_page.claims:
            claim_list = self.current_page.claims.get(prop)
            for claim in claim_list:
                for source in claim.sources:
                    changed = False
                    # Only process if source has "imported from Wikimedia project (P143) Minor Planet Center (Q522039)"
                    if not self.imported_from in source:
                        continue
                        
                    if not source[self.imported_from][0].target_equals(self.imported_from_old_val):
                        continue

                    # Multiple values are not expected
                    if len(source[self.imported_from]) > 1:
                        pywikibot.warn(f"Reference on {self.current_page.title()} property '{prop}' has multiple values for '{self.imported_from}'")
                        continue

                    pywikibot.output(f"Processing {prop}")

                    # Only add the id to the source if there's not already one there 
                    if self.stated_in not in source:
                        source[self.stated_in] = [self.new_claim(self.stated_in, self.stated_in_new_val, is_reference=True)]

                    # only remove the 'imported from' value if  the expected 'stated in' is present
                    if source[self.stated_in][0].target_equals(self.stated_in_new_val):
                        del source[self.imported_from]
                        changed=True
                    else:
                        pywikibot.warn(f"Reference on {self.current_page.title()} property '{prop}' has unexpected value for '{self.imported_from}'")

                    if changed:
                        changed_count += 1

        if not changed_count: return True 

        summary = f"Correcting Minor Planet Center references (×{changed_count})"

        # 'Always update' option is assumed to indicate bot operation
        if "always" in self.options and self.options["always"]:
            summary += ". See [[Wikidata:Requests_for_permissions/Bot/William_Avery_Bot_10|request for permission]]"
            self.current_page.editEntity(summary=summary)
        else:
            self.user_edit_entity(self.current_page, summary=summary)
        return True 
        
def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {}

    local_args = pywikibot.handle_args(args)

    gen_factory = pagegenerators_ext.GeneratorFactory()

    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(":")
        option = arg[1:]
        options[option] = True

    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        options["generator"] = gen
        bot = FixMpcRefs(**options)
        bot.run() 
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == '__main__':
    main()
