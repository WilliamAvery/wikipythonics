#!/bin/bash
python3 $PYWIKIBOT_DIR/pwb.py draftPickSortBot.py \
-mysqlquery:"\
select p.page_namespace, p.page_title \
from page p \
where exists (\
   select 1  from categorylinks cl2 \
             join category c2 on cl2.cl_to=c2.cat_title \
             where cl2.cl_from=p.page_id and c2.cat_title='Lists_of_National_Football_League_draftees_by_college_football_team') \
and exists (\
   select 1  from templatelinks tl \
             where tl.tl_from = p.page_id and tl.tl_title='Chronological' and tl.tl_namespace=10) \
order by p.page_title" \
 -limit:9999 -remove_template -simulate -report_page:User:William_Avery_Bot/tablesortsample # -report_only
