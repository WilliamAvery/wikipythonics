"""
Templates {{R to scientific name}} and  {{R from scientific name}} can take a parameter 
('fish', 'insect', etc) to subcategorise redirects from scientific names of organisms to
their common names.

The parameter is often not present in cases where it could usefully be supplied.
 
This bot will add the parameter, where the correct value can be determined from the taxobox
of the article that is the target of the redirect.

Command-line arguments:

-start           start title

-end             end at title

-limit           page limit to pass to generator

-audit           output some processing information

-always          always accept edits

-redirecttype    tosci   - process {{R to scientific name}}
                 fromsci - process {{R from scientific name}} (default)

Example:

    python3 redirectClassifierBot.py -start:B -end:C -simulate
"""
#
# (C) William Avery, 2020, 2021
# Distributed under the terms of the MIT license.
#
import sys
import requests

import mwparserfromhell
from bs4 import BeautifulSoup
import pywikibot

from pywikibot.bot import RedirectPageBot, ExistingPageBot


class redirectClassifierBot(RedirectPageBot, ExistingPageBot):
    def __init__(self, generator, **kwargs):
        self.available_options.update(
            {
                "start": None,
                "end": None,
                "limit": None,
                "audit": None,
                "always": None,
                "redirecttype": "fromsci",
            }
        )

        super(redirectClassifierBot, self).__init__(generator=generator, **kwargs)
        self.audit = "audit" in kwargs and kwargs["audit"]

    def setup(self):
        self.counters = dict({"others": 0})

    def teardown(self):
        pywikibot.output("======== RUN COUNTERS ========")
        pywikibot.output(self.counters)

    def treat_page(self):
        template_aliases = {
            "fromsci": [
                "R from scientific name",  # Items below are redirects
                "Rsci",
                "Redirect from scientific name",
                "R from biological name",
                "Redirect from biological name",
                "Redirect from astronomical name",
                "R from scientific title",
                "Redirect from scientific title",
                "R from binomial name",
                "Redirect from binomial name",
                "R from species name",
                "Redirect from species name",
                "R to non-scientific name",
                "Redirect to non-scientific name",
                "R to non-scientific title",
                "Redirect to non-scientific title",
                "R from order",
                "Redirect from order",
                "R from biological member",
                "Redirect from biological member",
                "R to common name",
                "Redirect from nomen erratum",
                "R to common",
                "Redirect to common name",
                "R sci",
            ],
            "tosci": [
                "R to scientific name",  # Items below are redirects
                "R from common name",
                "Redirect to scientific name",
                "R to biological name",
                "Redirect to biological name",
                "R to astronomical name",
                "Redirect to astronomical name",
                "R to scientific title",
                "Redirect to scientific title",
                "R from non-scientific name",
                "Redirect from non-scientific name",
                "R from non-scientific title",
                "Redirect from non-scientific title",
                "Rtsci",
                "R from vernacular name",
                "Redirect from vernacular name",
                "R to scientific",
                "Redirect from common name",
            ],
            "fromalt": [
                "R from alternative scientific name",   # Items below are redirects
                "R from taxonomic synonym",
                "Raltsci",
                "R asn",
                "Redirect asn",
                "R to alternative scientific name",
                "Redirect from biological synonym",
                "R from another scientific name",
                "Redirect from alternative scientific name",
                "Redirect from taxonomic synonym",
                "Redirect to alternative scientific name",
                "R alt taxonomy",
                "R from alternate scientific name",
                "R from scientific synonym",
            ],
            "frommono": [
                "R from monotypic taxon"   # Items below are redirects
                "R from mt",
                "Redirect from monotypic taxon"
            ],
            "tomono": [
                "R to monotypic taxon",    # Item below is redirect
                "R to mt"
            ]
        }

        def get_taxon(soupdoc, level):
            taxon_str = ""
            taxon_elem = soupdoc.select_one(
                f"table.infobox.biota tr td:-soup-contains('{level}:') + td a"
            )
            if not taxon_elem:
                taxon_elem = soupdoc.select_one(
                    f"table.infobox.biota tr td:-soup-contains('{level}:') + td"
                )
            if taxon_elem:
                taxon_str = str(taxon_elem.get_text().strip())
            return taxon_str

        def debugprint(txt):
            if self.audit:
                pywikibot.output(txt)

        page_t = self.current_page.title()

        with requests.get(
            url=f"https://en.wikipedia.org/api/rest_v1/page/html/{page_t}"
        ) as fp:
            if fp.status_code != 200:
                pywikibot.warning(f"request status {fp.status_code} on {page_t}")
            else:

                soup = BeautifulSoup(fp.content, features="lxml")
                # Get values from the taxonbox
                unranked = get_taxon(
                    soup, "(unranked)"
                )  # returns the first 'unranked' line
                domain = get_taxon(soup, "Domain")
                regnum = get_taxon(soup, "Kingdom")
                divisio = get_taxon(soup, "Division")
                phylum = get_taxon(soup, "Phylum")
                subphylum = get_taxon(soup, "Subphylum")
                classis = get_taxon(soup, "Class")
                ordo = get_taxon(soup, "Order")

                # Determine required parameter value from taxonomy
                ptag = ""
                if unranked == "Virus":
                    ptag = "virus"
                if domain == "Bacteria" or regnum == "Bacteria":
                    ptag = "microorganism"
                if regnum == "Plantae":
                    ptag = "plant"
                if regnum == "Animalia":
                    ptag = "animal"
                if regnum == "Fungi":
                    ptag = "fungus"
                if divisio in ["Rhodophyta", "Glaucophyta"]:
                    ptag = "alga"
                if phylum == "Chlorophyta":
                    ptag = "alga"
                if phylum == "Arthropoda":
                    ptag = "arthropod"
                if phylum == "Mollusca":
                    ptag = "mollusk"
                if subphylum == "Crustacea":
                    ptag = "crustacean"
                if classis == "Actinopterygii" or classis == "Chondrichthyes":
                    ptag = "fish"
                if classis == "Amphibia":
                    ptag = "amphibian"
                if classis == "Aves":
                    ptag = "bird"
                if classis == "Mammalia":
                    ptag = "mammal"
                if classis == "Insecta":
                    ptag = "insect"
                if classis in [
                    "Bacillariophyceae",
                    "Phaeophyceae",
                    "Actinochrysophyceae",
                ]:
                    ptag = "alga"
                if ordo == "Testudines" or ordo == "Squamata" or ordo == "Crocodilia":
                    ptag = "reptile"
                if ordo == "Araneae":
                    ptag = "spider"

                debugprint(
                    "Regnum: {0} Subphylum: {1} Classis: {2} Ordo: {3} ptag={4}".format(
                        regnum, subphylum, classis, ordo, ptag
                    )
                )

                # Keep a count of what has been found
                if ptag:
                    if ptag in self.counters:
                        self.counters[ptag] += 1
                    else:
                        self.counters[ptag] = 1
                else:
                    self.counters["others"] += 1

                if ptag:
                    template_name = ""
                    ptext = self.current_page.get(get_redirect=True)
                    newtext = ptext
                    debugprint(ptext)
                    mwpfh = mwparserfromhell.parse(ptext)
                    for template in mwpfh.filter_templates():
                        if template.name.matches(
                            template_aliases[self.opt["redirecttype"]]
                        ):
                            template_name = str(template.name)
                            debugprint("Found " + template_name)
                            template.add(1, ptag)
                            newtext = str(mwpfh)
                            debugprint(newtext)
                    self.userPut(
                        self.current_page,
                        ptext,
                        newtext,
                        summary=f"Added parameter '{ptag}' to template {{{{[[template:{template_name}|{template_name}]]}}}}. See [[WP:Bots/Requests for approval/William Avery Bot 2|BRFA]]",
                    )
                soup.decompose()


def cat_gen(**options):
    topcat_map = {
        "fromsci": "Category:Redirects from scientific names",
        "tosci": "Category:Redirects to scientific names",
        "fromalt": "Category:Redirects from alternative scientific names",
        "frommono": "Category:Redirects from monotypic taxa",
        "tomono": "Category:Redirects to monotypic taxa"
    }

    cat = pywikibot.Category(pywikibot.Site(), topcat_map[options["redirecttype"]])
    return iter(
        cat.articles(
            startprefix=options["start"],
            endprefix=options["end"],
            total=options["limit"],
        )
    )


def main(*args):

    options = {"redirecttype": "fromsci"}
    options["start"] = options["end"] = options["limit"] = None

    local_args = pywikibot.handle_args(args)
    for arg in local_args:
        option, sep, value = arg.partition(":")
        if option == "-start":
            options["start"] = value
        elif option == "-end":
            options["end"] = value
        elif option == "-limit":
            options["limit"] = int(value)
        elif option == "-audit":
            options["audit"] = True
        elif option == "-always":
            options["always"] = True
        elif option == "-redirectype":
            if value not in ["fromsci", "tosci", "fromalt", "frommono", "tomono"]:
                pywikibot.error(f"bad value: {value} for option {option}")
                sys.exit(2)
            options["redirectype"] = value
        else:
            pywikibot.error(f"Unknown parameter: {option}")
            sys.exit(2)

    bot = redirectClassifierBot(cat_gen(**options), **options)
    bot.run()


if __name__ == "__main__":
    main()
