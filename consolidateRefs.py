#!/usr/bin/python
"""
Consolidate multiple references on the same claim that are stated in
Accademia delle Scienze di Torino website into one reference, as long
as there are no conflicting values
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2022
# (C) William Avery, 2022
#
# Distributed under the terms of the MIT license.
#

import json

import pywikibot
from pywikibot import pagegenerators
import pagegenerators_ext
from pywikibot.page import WikibasePage
from pywikibot.bot import WikidataBot

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {"&params;": pagegenerators.parameterHelp}


class ConsolidateRefs(
    WikidataBot,  # Only used to get user_edit_entity method for testing, treat_page is overridden
):
    def __init__(self, **kwargs):
        self.available_options.update({})

        super(ConsolidateRefs, self).__init__(**kwargs)

        self.options = kwargs

        self.stated_in = "P248"
        self.target = "Q107212659"  # Accademia delle Scienze di Torino

    def teardown(self):
        pass

    def treat_page(self) -> None:

        delete_count = 0
        conflict = False
        for prop in self.current_page.claims:
            claim_list = self.current_page.claims.get(prop)
            for claim in claim_list:
                first_source = None

                remove_source = []

                for source in claim.sources:
                    if self.stated_in in source and source[self.stated_in][
                        0
                    ].target_equals(self.target):
                        if not first_source:
                            first_source = source
                        else:
                            pywikibot.output(f"Processing ({prop})")
                            for source_prop in source:
                                if source_prop in first_source:
                                    if not first_source[source_prop][0].target_equals(
                                        source[source_prop][0].getTarget()
                                    ):
                                        pywikibot.warn(
                                            f'{source_prop} conflict. "{first_source[source_prop][0].getTarget()}" <> "{source[source_prop][0].getTarget()}" '
                                        )
                                        conflict = True
                                        break
                                else:
                                    first_source[source_prop] = source[source_prop]
                            else:
                                # insert higher indices first, so indices don't change on deletion
                                remove_source.insert(0, claim.sources.index(source))
                for source in remove_source:
                    del claim.sources[source]
                    delete_count += 1

        if conflict or not delete_count:
            return True

        summary = f"Consolidating Accademia delle Scienze di Torino multiple references (-{delete_count})"

        # 'Always update' option is assumed to indicate bot operation
        if "always" in self.options and self.options["always"]:
            summary += ". See [[Wikidata:Requests_for_permissions/Bot/William_Avery_Bot_7|request for permission]]"
            self.current_page.editEntity(summary=summary)
        else:
            self.user_edit_entity(self.current_page, summary=summary)


def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {}

    local_args = pywikibot.handle_args(args)

    gen_factory = pagegenerators_ext.GeneratorFactory()

    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(":")
        option = arg[1:]
        options[option] = True

    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        options["generator"] = gen
        bot = ConsolidateRefs(**options)
        bot.run()
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == "__main__":
    main()
