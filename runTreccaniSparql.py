import pywikibot
import pymysql as mariadb
import configparser
query = """
SELECT DISTINCT ?item WHERE {
  {
    SELECT DISTINCT ?item WHERE {
      ?item p:P31 ?statement0.
      ?statement0 (ps:P31) wd:Q5.
      {
        ?item p:P4223 ?statement1.
        ?statement1 (ps:P4223) _:anyValueP3365
      }
    }
    LIMIT 100000
  }
}"""
site=None
item_name= 'item'
endpoint= None
entity_url = None,
result_type=set
from pywikibot.data import sparql

if site is None:
    site = pywikibot.Site()
repo = site.data_repository()
dependencies = {'endpoint': endpoint, 'entity_url': entity_url}
if not endpoint or not entity_url:
    dependencies['repo'] = repo
query_object = sparql.SparqlQuery(**dependencies)

conn = mariadb.connect(
  host="localhost",
  port=4711,
  database="s54354__batchcontrol",
  read_default_file ="~/batch.extra"
)
gencurr = conn.cursor()

data = query_object.get_items(query,
                                  item_name=item_name,
                                  result_type=result_type)
for item in data:
    print(item) 
    gencurr.execute(
        f"INSERT IGNORE INTO treccani_batch (item, scheduled_time) VALUES ('{item}', '2232-01-01 00:00:00')")
    gencurr.execute('COMMIT')
    
