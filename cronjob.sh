#!/bin/bash
#
# (C) William Avery
#
# Distributed under the terms of the MIT license.
#
# Runs William_Avery_Bot tasks as a consolidated cronjob
#
echo ---- Running TaxonbarSyncer ----------------------------------------------
python3 - -pt:2 <<FILEEND
import sys
from mail import SendMailSession  
import pywikibot
from pywikibot import config
from pywikibot.pagegenerators import MySQLPageGenerator

pywikibot.handle_args()

try:
    import pagegenerators_ext as pagegenerators_ext
    from taxonbarSyncerBot import taxonbarSyncer
    taxonbarSyncer(
        generator=MySQLPageGenerator(
            query="select page_namespace, page_title from categorylinks join page on cl_from=page_id join revision on rev_id=page_latest where cl_to = 'Taxonbars_desynced_from_Wikidata' and timestamp(rev_timestamp) >   current_timestamp - INTERVAL 1 HOUR and timestamp(rev_timestamp) <   current_timestamp - INTERVAL 30 MINUTE",
        ),
    ).run()

except Exception as e:
    SendMailSession().sendException("TaxonbarSyncer failed", sys.exc_info())
FILEEND
echo ---- Ran TaxonbarSyncer ---------------------------------------------------
echo ---- Running CleanseRefs---------------------------------------------------
python3 - -pt:2 -dir:~/toolsbot <<FILEEND
import sys
from mail import SendMailSession 
try:
    import pywikibot
    import pagegenerators_ext as pagegenerators_ext
    import batchfillers
    from cleanseRefs import CleanseRefs

    batchfillers.huntTrackers()

    CleanseRefs(
        generator=pagegenerators_ext.MySQLItemBatch(
            query="select item from cleanse_refs where NOW() >= scheduled_time and processed_time is null",
            dbname="s54354__batchcontrol",
            posttreatproc="cleanserefs_tr",
        ),
        always=True,
    ).run()

except Exception as e:
    SendMailSession().sendException("CleanseRefs failed", sys.exc_info())
FILEEND
echo ---- Ran CleanseRefs ------------------------------------------------------
