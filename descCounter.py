#!/usr/bin/python
"""
Count astronomical objects with space missing from Dutch description
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2024
# (C) William Avery, 2024
#
# Distributed under the terms of the MIT license.
#

import sys
from SPARQLWrapper import SPARQLWrapper, JSON
import pywikibot
from pywikibot import pagegenerators
import pagegenerators_ext
from pywikibot.bot import WikidataBot
from pywikibot.page import Page

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {"&params;": pagegenerators.parameterHelp}


class descCounter(
    WikidataBot,  # Only used to get user_edit_entity method for testing, treat_page is overridden
):
    def __init__(self, **kwargs):
        self.available_options.update({})

        super(descCounter, self).__init__(**kwargs)

        self.options = kwargs
        self.count = 0
        self.numstyle = "style = ""text-align:right;"""
        self.output = """
{| class="wikitable sortable"
|+ Count of items that lack a space in their Dutch description
|-
! Constellation item !! Dutch Name !! {self.numstyle} | Count
"""

    def teardown(self):
        self.output += f"""|-
! !! style= "text-align:left" | Total !! {self.numstyle} | {self.count}
|}}"""
        report_page = Page(pywikibot.Site(), "User:William_Avery/nldesc_nospace")
        report_page.text = self.output
        report_page.save("Count of nl descriptions missing a space")

    def treat_page(self) -> None:

        title = self.current_page.title()
        print(self.current_page.title())
        if 'en' in self.current_page.labels:
            print(self.current_page.labels['en'])
        if 'nl' in self.current_page.labels:
            print(self.current_page.labels['nl'])

            query = f"""SELECT
    (COUNT(?item) AS ?count)
    WHERE {{
    ?item wdt:P59 wd:{title}.
    ?item schema:description ?itemDescription .
    FILTER(LANG(?itemDescription) = "nl")
    FILTER(STRENDS(?itemDescription, 'sterrenbeeld{self.current_page.labels['nl']}')) .
    }}
    """
            results = get_results(query)

            count = results["results"]["bindings"][0]['count']['value']
            self.count += int(count)
            self.output += f"""|-
| {{{{Q|{title[1:]}}}}} || {self.current_page.labels['nl']} || style = "text-align:right;"| {count}
"""



def get_results(query):
        endpoint_url = "https://query.wikidata.org/sparql"
        user_agent = "User:William_Avery Python/%s.%s" % (sys.version_info[0], sys.version_info[1])
        # TODO adjust user agent; see https://w.wiki/CX6
        sparql = SPARQLWrapper(endpoint_url, agent=user_agent)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        return sparql.query().convert()



def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {}

    local_args = pywikibot.handle_args(args)

    gen_factory = pagegenerators_ext.GeneratorFactory()

    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(":")
        option = arg[1:]
        options[option] = True

    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        options["generator"] = gen
        bot = descCounter(**options)
        bot.run()
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == "__main__":
    main()
