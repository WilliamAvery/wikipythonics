#!/usr/bin/python
"""
Change http://runeberg.org URLs to https
"""
#
# Derived from Basic.py (C) Pywikibot team, 2006-2024
# (C) William Avery, 2024
#
# Distributed under the terms of the MIT license.
#

import json

import pywikibot
from pywikibot import pagegenerators
import pagegenerators_ext
from pywikibot.page import WikibasePage
from pywikibot.bot import WikidataBot

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {"&params;": pagegenerators.parameterHelp}


class FixRuneberg(
    WikidataBot,  # Only used to get user_edit_entity method for testing, treat_page is overridden
):
    def __init__(self, **kwargs):
        self.available_options.update({})

        super(FixRuneberg, self).__init__(**kwargs)

        self.options = kwargs

        self.target_domain = "http://runeberg.org" 

    def teardown(self):
        pass

    def treat_page(self) -> None:

        edit_count = 0
        edit_count = self.process_claims(edit_count, self.current_page.claims)
        for prop in self.current_page.claims:
            claim_list = self.current_page.claims.get(prop)
            for claim in claim_list:
                for source in claim.sources:
                    edit_count = self.process_claims(edit_count, source)
                edit_count = self.process_claims(edit_count, claim.qualifiers)
        summary = f"Changing runeberg.org URLs to https (×{edit_count})"

        if not edit_count: return

        # 'Always update' option is assumed to indicate bot operation
        if "always" in self.options and self.options["always"]:
            summary += ". See [[Wikidata:Requests_for_permissions/Bot/William_Avery_Bot_11|request for permission]]"
            self.current_page.editEntity(summary=summary)
        else:
            self.user_edit_entity(self.current_page, summary=summary)

    def process_claims(self, edit_count, source):

        for claim in source:
            print(claim)
            claimlist = source[claim]
            for (url_claim) in claimlist:
                if (url_claim.type == 'url'):
                    if url_claim.target.startswith(self.target_domain):
                        url_claim.setTarget('https' + url_claim.target[4:])
                        edit_count += 1
        return edit_count

def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {}

    local_args = pywikibot.handle_args(args)

    gen_factory = pagegenerators_ext.GeneratorFactory()

    local_args = gen_factory.handle_args(local_args)

    # Parse command line arguments
    for arg in local_args:
        arg, sep, value = arg.partition(":")
        option = arg[1:]
        options[option] = True

    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        options["generator"] = gen
        bot = FixRuneberg(**options)
        bot.run()
    else:
        pywikibot.bot.suggest_help(missing_generator=True)


if __name__ == "__main__":
    main()
