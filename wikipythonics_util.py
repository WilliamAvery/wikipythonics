"""
Module to apply some general fixes.

I thought it would be as simple to remove redirects and alternative parameter names as
it would be to work around them in the bot's code, but if the bot then removes some of 
the fixed items it invalidates the edit summary.

Currently the only entrypoint is enwiki_apply_fixes
The only fix applied at present is bypassing template redirects
Does not restrict its actions per MediaWiki namespaces
"""
#
# (C) William Avery 2021
#
# Distributed under the terms of the MIT license.
# No AWB code (GPL licensed) was consulted in writing this module
#
from pathlib import Path
import pywikibot
from pywikibot.page import Page
import mwparserfromhell
_template_redirect_map = {}
def _populatetplmap():
    """
    Parse the AWB rules for bypassing template redirects to build a map that can
    easily be used in applying fixes 
    """
    template_redirect_page = Page(pywikibot.Site('en'), 'Wikipedia:AutoWikiBrowser/Template redirects')
    tred_mwtree = mwparserfromhell.parse(template_redirect_page.text)
    rulesection = tred_mwtree.get_sections(matches='Main and Category namespace rules')

    redirect_list = []
    expect_last = False
    for mwpnode in rulesection[0].filter(recursive=True):
         if mwpnode == '\n':  # Reset variables at new line
            redirect_list = []
            expect_last = False
         elif str(mwpnode).strip() == '→': # What follows is the real template name
            expect_last = True
         elif type(mwpnode) == mwparserfromhell.nodes.template.Template and mwpnode.name=='tl':
            if not expect_last:
                redirect_list.append(str(mwpnode.params[0].strip()))
            else:
                for rd in redirect_list: _template_redirect_map[rd] = str(mwpnode.params[0].strip())
    pywikibot.debug(f"_tpl_map=\n{str(_template_redirect_map)}", 'tpl_map')

def capitalize_firstchar(name):
    """
    Replace a string's initial letter with uppercase and, 
    unlike python capitalize(), leave the rest of the string untouched
    """
    return name[:1].upper() + name[1:]

def decapitalize(name):
    "Replace a string's initial capital with lowercase"
    return name[:1].lower() + name[1:]

def enwiki_apply_fixes(mwtree):
    "The only fix applied at present is bypassing template redirects"

    pywikibot.output(f"{pywikibot.calledModuleName()} applying general fixes")
 
    fix_count = 0 
    if not _template_redirect_map:
        _populatetplmap()
    for template in mwtree.filter_templates():
        old_template_name = template.name.strip()
        template_map_key = old_template_name.replace('_', ' ')
        tpl_has_underscores = (template_map_key != old_template_name) # take hint, if present
        template_map_key = capitalize_firstchar(template_map_key)
        if template_map_key in _template_redirect_map:
            fix_count += 1
            new_name = _template_redirect_map[template_map_key]
            if tpl_has_underscores:
                new_name = new_name.replace(' ', '_')
            if old_template_name[:1].lower()==old_template_name[:1] and new_name[:3].upper() != new_name[:3]:
                new_name = decapitalize(new_name)
            template.name = old_template_name.replace(old_template_name, new_name)

    return f"; bypassed {fix_count} redirect(s)" if fix_count else ''